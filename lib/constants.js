"use strict";

module.exports.any = {};
module.exports.anyOrNone = {};
module.exports.anyOrNoneToEnd = {};

module.exports.modes = ["sequential", "greedy"];
module.exports.maxLenOfArgsPrint = 100;
