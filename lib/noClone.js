"use strict";

var _ = require("lodash");
var validate = require("./validate").noClone;
var NoCloneWrapper = require("./NoCloneWrapper");

module.exports = function(value) {
    validate.args(_.toArray(arguments));

    return new NoCloneWrapper(value);
};
