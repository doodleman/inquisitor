"use strict";

require("./automaticRestoreVerifyAndReset");

var _ = require("lodash");
var state = require("./state");

module.exports.Sequence = require("./Sequence");
module.exports.ExpectationError = require("./ExpectationError");

module.exports.any = require("./constants").any;
module.exports.anyOrNone = require("./constants").anyOrNone;
module.exports.anyOrNoneToEnd = require("./constants").anyOrNoneToEnd;

module.exports.noClone = require("./noClone");
module.exports.NoCloneWrapper = require("./NoCloneWrapper");

module.exports.expect = require("./expect");
module.exports.isMocked = require("./mocks").isMocked;
module.exports.configure = require("./mocks").configure;

module.exports.createMockFunction = require("./mocks").createMockFunction;
module.exports.createMockObject = require("./mocks").createMockObject;
module.exports.mock = require("./mocks").mock;
module.exports.mockify = require("./mocks").mockify;

module.exports.use = function(plugin) {
    if (_.includes(state.usedPlugins, plugin) === false) {
        plugin(module.exports);
        state.usedPlugins.push(plugin);
    }
};
