"use strict";

var WeakMap = require("es6-weak-map");
var Set = require("es6-set");

module.exports.registeredMocks = new WeakMap; // All active mocks
module.exports.registeredMocksToRestore = [];
module.exports.registeredMocksToVerify = [];
module.exports.registeredMocksToReset = new Set;

module.exports.testsStarted = false;
module.exports.usedPlugins = [];
module.exports.expectationErrors = [];
