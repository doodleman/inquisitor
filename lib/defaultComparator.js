"use strict";

var _ = require("lodash");

function compare(arg1, arg2) {
    if (Buffer.isBuffer(arg1) && Buffer.isBuffer(arg2)) {
        return arg1.equals(arg2);
    }
    return _.isEqual(arg1, arg2);
}

module.exports = compare;
