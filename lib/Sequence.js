"use strict";

function Sequence() {
    this.lastExpectation = null;
    this.lastExpectationCall = undefined;
}

Sequence.prototype.extend = function(expectation, beginCall, endCall) {
    if (this.lastExpectation !== null) {
        if (Array.isArray(expectation.after[beginCall]) === false) {
            expectation.after[beginCall] = [];
        }
        expectation.after[beginCall].push({expectation: this.lastExpectation, expectedCall: this.lastExpectationCall});
    }

    this.lastExpectation = expectation;
    this.lastExpectationCall = endCall;
};

module.exports = Sequence;
