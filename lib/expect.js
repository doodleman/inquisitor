"use strict";

var _ = require("lodash");

var state = require("./state");
var utils = require("./utils");
var validate = require("./validate").expect;
var MethodMock = require("./mocks").MethodMock;
var Expectation = require("./Expectation");

module.exports = function(mockedFunction) {
    validate.callability();
    validate.args(mockedFunction);
    var mock = state.registeredMocks.get(mockedFunction);

    if (_.isEmpty(mock.expectations)) {
        state.registeredMocksToVerify.push(mock);

        if (!(mock instanceof MethodMock)) {
            state.registeredMocksToReset.add(mock);
        }
    }

    var expectation = new Expectation(mock);
    utils.saveStacks(expectation);
    mock.expectations.push(expectation);

    return expectation;
};
