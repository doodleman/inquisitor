"use strict";

var validate = require("./validate").base;
var utils = require("./utils");

function Base() {
    this.comparatorFunction = undefined;
    this.comparatorContext = undefined;
    this.comparatorContextIsSet = false;

    this.returnValue = undefined;
    this.returnValueIsSet = false;

    this.callbackFunction = undefined;
    this.callbackContext = undefined;
    this.callbackContextIsSet = false;
}

Base.prototype.comparator = function(comparatorFunction, comparatorContext) {
    var mock = utils.getMock(this);
    validate.comparator.callability(this);
    validate.comparator.args(comparatorFunction);

    this.comparatorFunction = comparatorFunction;
    if (arguments.length > 1) {
        this.comparatorContext = comparatorContext;
        this.comparatorContextIsSet = true;
    }

    utils.addToReset(mock);

    return this;
};

Base.prototype.compareWith = Base.prototype.comparator;

Base.prototype.return = function(returnValue) {
    var mock = utils.getMock(this);
    validate.return.callability(this);

    this.returnValue = returnValue;
    this.returnValueIsSet = true;

    utils.addToReset(mock);

    return this;
};

Base.prototype.returns = Base.prototype.return;

Base.prototype.callback = function(callbackFunction, callbackContext) {
    var mock = utils.getMock(this);
    validate.callback.callability(this);
    validate.callback.args(mock, callbackFunction);

    this.callbackFunction = (callbackFunction === "original") ? mock.originalMethod : callbackFunction;

    if (arguments.length > 1) {
        this.callbackContext = callbackContext;
        this.callbackContextIsSet = true;
    }

    utils.addToReset(mock);

    return this;
};

Base.prototype.call = Base.prototype.calls = Base.prototype.callback;

module.exports = Base;
