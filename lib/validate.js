"use strict";

//todo maybe find some nice npm module for all this arguments validation?

var _ = require("lodash");

var utils = require("./utils");

var constants = require("./constants");
var state = require("./state");

var anyOrNone = constants.anyOrNone;
var anyOrNoneToEnd = constants.anyOrNoneToEnd;

var Sequence = require("./Sequence");

function validatePositiveInteger(value, valueDescription) {
    if (!_.isNumber(value)) {
        throw new TypeError(valueDescription + " must be number.");
    }

    if (!utils.isInt(value)) {
        throw new TypeError(valueDescription + " must be integer.");
    }

    if (value <= 0) {
        throw new RangeError(valueDescription + " must be greater than 0.");
    }
}

function validateExpectationCallArg(expectation, arg, argDescription) {
    validatePositiveInteger(arg, argDescription);

    if (arg > expectation.expectedMaxCallCount) {
        throw new RangeError(argDescription + " cannot be greater than the number of expected calls.");
    }
}

function validateExpectationCallsCount(count, methodName, whichArg) {
    whichArg = (whichArg === undefined ? "" : (whichArg + " "));

    if (typeof count !== "number") {
        throw new TypeError("'" + methodName + "' expects number as " + whichArg + "parameter.");
    }

    if (utils.isInt(count) === false) {
        throw new TypeError("'" + methodName + "' expects integer as " + whichArg + "parameter.");
    }

    if (count < 0) {
        throw new RangeError("'" + methodName + "' expects non-negative integer as " + whichArg + "parameter.");
    }
}

function validateForNever(expectation, methodName) {
    if (expectation.mock.expectations.length !== 1) {
        throw new Error("'" + methodName + "' cannot be called when other expectations were already set.");
    }

    if (expectation.expectedArgsIsSet) {
        throw new Error("'" + methodName + "' cannot be called along with 'args'/'argsWhich'.");
    }
}

function validateArgsCallability(expectation, methodName) {
    if (expectation.expectedArgsIsSet) {
        throw new Error("Expected arguments cannot be set twice.");
    }

    if (expectation.expectedMaxCallCount === 0) {
        throw new Error("'" + methodName + "' cannot be called along with 'never'/'exactly(0)/most(0)'.");
    }
}

function validateBaseMethodCallability(base, isAlreadySet, valueSet, methodNameAndAliases) {
    if (valueSet === "Callback" && valueSet === "Return value" && !state.testsStarted) {
        throw new Error(methodNameAndAliases + " cannot be used before tests start. Use default version.");
    }

    if (isAlreadySet) {
        throw new Error(valueSet + " cannot be set twice.");
    }

    if (utils.isMock(base) && !_.isEmpty(base.expectations)) {
        throw new Error(methodNameAndAliases + " must be used before any expectations are set.");
    }
}

function validateCallbackArgs(mock, callbackFunction, methodNameAndAliases) {
    if (mock.originalMethod !== undefined) {
        if (!_.isFunction(callbackFunction) && callbackFunction !== "original") {
            throw new TypeError(methodNameAndAliases + " expects function or string 'original' as first parameter.");
        }
    }
    else {
        if (!_.isFunction(callbackFunction)) {
            throw new TypeError(methodNameAndAliases + " expects function as first parameter.");
        }
    }
}

var base = {
    comparator: {
        callability: function(base) {
            validateBaseMethodCallability(base, base.comparatorFunction !== undefined, "Comparator", "'comparator'/'compareWith'");
        },

        args: function(comparatorFunction) {
            if (!_.isFunction(comparatorFunction)) {
                throw new TypeError("'comparator'/'compareWith' expects function as first parameter.");
            }
        }
    },

    return: {
        callability: function(base) {
            validateBaseMethodCallability(base, base.returnValueIsSet, "Return value", "'return'/'returns'");
        }
    },

    callback: {
        callability: function(base) {
            validateBaseMethodCallability(base, base.callbackFunction !== undefined, "Callback", "'callback'/'call'/'calls'");
        },

        args: function(mock, callbackFunction) {
            validateCallbackArgs(mock, callbackFunction, "'callback'/'call'/'calls'");
        }
    }
};

var expect = {
    callability: function() {
        if (!state.testsStarted) {
            throw new Error("'expect' cannot be used before tests start.");
        }
    },

    args: function(mockedFunction) {
        if (!_.isFunction(mockedFunction) || !state.registeredMocks.has(mockedFunction)) {
            throw new Error("'expect' expects mocked function as parameter.");
        }

        var mock = state.registeredMocks.get(mockedFunction);

        if (!_.isEmpty(mock.expectations) && mock.expectations[0].expectedMaxCallCount === 0) {
            throw new Error("Cannot set another expectation if previously used 'never'/'exactly(0)'/'most(0)'.");
        }
    }
};

var mocks = {
    isMocked: {
        args: function(fn) {
            if (!_.isFunction(fn)) {
                throw new TypeError("'isMocked' expects function as parameter.");
            }
        }
    },

    configure: {
        args: function(mockedFunction) {
            if (!_.isFunction(mockedFunction) || !state.registeredMocks.has(mockedFunction)) {
                throw new Error("'configure' expects mocked function as parameter.");
            }
        }
    },

    createMockFunction: {
        args: function(name) {
            if (name !== undefined && typeof name !== "string") {
                throw new TypeError("'createMockFunction' expects string or nothing as parameter.");
            }
        }
    },

    createMockObject: {
        args: function(methodNames) {
            if (_.some(methodNames, function(methodName) { return typeof methodName !== "string"; })) {
                throw new TypeError("'createMockObject' expects only strings as parameters.");
            }

            if (_.uniq(methodNames).length !== methodNames.length) {
                throw new Error("'createMockObject' cannot create object with duplicated keys.");
            }
        }
    },

    mock: {
        callability: function(mockedMethodName) {
            if (!state.testsStarted) {
                throw new Error("Cannot mock '" + mockedMethodName + "'. Don't mock before tests start.");
            }
        },

        args: function(mockedObject, mockedMethodName) {
            if (!_.isObject(mockedObject)) {
                throw new TypeError("'mock' expects object as first parameter.");
            }

            if (typeof mockedMethodName !== "string") {
                throw new TypeError("'mock' expects string as second parameter.");
            }

            if (!(mockedMethodName in mockedObject)) {
                throw new ReferenceError("Given object has no method '" + mockedMethodName + "'.");
            }

            if (!_.isFunction(mockedObject[mockedMethodName])) {
                throw new TypeError("Property '" + mockedMethodName + "' of given object isn't function.");
            }

            if (state.registeredMocks.has(mockedObject[mockedMethodName])) {
                throw new Error("Cannot mock '" + mockedMethodName + "'. Method was already mocked in this test case.");
            }
        }
    },

    mockify: {
        callability: function() {
            if (!state.testsStarted) {
                throw new Error("Cannot mockify object. Don't mock before tests start.");
            }
        },

        args: function(mockedObject, depth) {
            if (!_.isObject(mockedObject)) {
                throw new TypeError("'mockify' expects object as first parameter.");
            }

            if (depth !== undefined) {
                validatePositiveInteger(depth, "'Depth' parameter");
            }
        }
    },

    mode: {
        callability: function(mock) {
            validateBaseMethodCallability(mock, mock.modeValueIsSet, "Mode", "'mode'");
        },

        args: function(mode) {
            if (!_.includes(constants.modes, mode)) {
                throw new Error("'mode' expects a correct mode value. Available modes: " + constants.modes.join(", ") + ".");
            }
        }
    },

    defaultReturn: {
        callability: function(mock) {
            validateBaseMethodCallability(mock, mock.defaultReturnValueIsSet, "Default return value", "'defaultReturn'/'byDefaultReturns'");
        }
    },

    defaultCallback: {
        callability: function(mock) {
            validateBaseMethodCallability(mock, mock.defaultCallbackFunction !== undefined, "Default callback", "'defaultCallback'/'byDefaultCall'/'byDefaultCalls'");
        },

        args: function(mock, callbackFunction) {
            validateCallbackArgs(mock, callbackFunction, "'defaultCallback'/'byDefaultCall'/'byDefaultCalls'");
        }
    }
};

var expectation = {
    least: {
        callability: function(expectation) {
            if (expectation.expectedMinCallCountIsSet === true) {
                throw new Error("Minimum number of expected calls cannot be set twice.");
            }
        },

        args: function(expectation, count) {
            validateExpectationCallsCount(count, "least");

            if (count > expectation.expectedMaxCallCount) {
                throw new RangeError("Minimum number of expected calls cannot exceed maximum number.");
            }
        }
    },

    most: {
        callability: function(expectation) {
            if (expectation.expectedMaxCallCountIsSet === true) {
                throw new Error("Maximum number of expected calls cannot be set twice.");
            }
        },

        args: function(expectation, count) {
            validateExpectationCallsCount(count, "most");

            if (count < expectation.expectedMinCallCount) {
                throw new RangeError("Maximum number of expected calls cannot be less than minimum number.");
            }

            if (count === 0) {
                validateForNever(expectation, "most(0)");
            }
        }
    },

    between: {
        callability: function(exp) {
            expectation.least.callability(exp);
            expectation.most.callability(exp);
        },

        args: function(expectation, from, to) {
            validateExpectationCallsCount(from, "between", "first");
            validateExpectationCallsCount(to, "between", "second");

            if (from > to) {
                throw new RangeError("Minimal number of expected calls cannot exceed maximum number.");
            }

            if (to === 0) {
                validateForNever(expectation, "between(0,0)");
            }
        }
    },

    exactly: {
        callability: function(exp) {
            if (exp.expectedMinCallCountIsSet && exp.expectedMaxCallCountIsSet) {
                throw new Error("Number of expected calls cannot be set twice.");
            }

            expectation.least.callability(exp);
            expectation.most.callability(exp);
        },

        args: function(expectation, count) {
            validateExpectationCallsCount(count, "exactly");

            if (count === 0) {
                validateForNever(expectation, "exactly(0)");
            }
        }
    },

    args: {
        callability: function(expectation) {
            validateArgsCallability(expectation, "args");
        },

        args: function(args) {
            if (_.initial(args).indexOf(anyOrNoneToEnd) !== -1) {
                throw new Error("anyOrNoneToEnd can only be the last parameter in 'args'.");
            }

            // by "regular" I mean neither 'anyOrNone' nor 'anyOrNoneToEnd'
            var lastRegular = _.findLastIndex(function(arg) {
                return (arg !== anyOrNone && arg !== anyOrNoneToEnd);
            });

            if (_.includes(args.slice(0, lastRegular + 1), anyOrNone)) {
                throw new Error("there can be no anyOrNone before regular arguments in 'args'.");
            }
        }
    },

    argsWhich: {
        callability: function(expectation) {
            validateArgsCallability(expectation, "argsWhich");
        },

        args: function(predicate) {
            if (!_.isFunction(predicate)) {
                throw new TypeError("'argsWhich' expects function as first parameter.");
            }
        }
    },

    in: {
        callability: function(expectation) {
            if (expectation.expectedCallCountIsSet === false) {
                throw new Error("'in' cannot be called before number of expected calls is set.");
            }
        },

        args: function(args) {
            if (_.isEmpty(args)) {
                throw new TypeError("'in' expects at least one instance of 'Sequence' class as parameters.");
            }

            if (args.some(function(sequence) { return !(sequence instanceof Sequence); })) {
                throw new TypeError("'in' only accepts instances of 'Sequence' class as parameters.");
            }
        }
    },

    toPromise: {
        callability: function(expectation) {
            if (expectation.expectedCallCountIsSet === false) {
                throw new Error("'toPromise' cannot be used before number of expected calls is set.");
            }
        },

        args: function(expectation, calls) {
            if ((calls !== undefined) && (typeof calls !== "number") && (Array.isArray(calls) === false)) {
                throw new TypeError("'toPromise' expects no parameters or number or array as parameter.");
            }

            if (typeof calls === "number") {
                validateExpectationCallArg(expectation, calls, "Number passed to 'toPromise'");
            }

            if (Array.isArray(calls)) {
                calls.forEach(function(call) {
                    return validateExpectationCallArg(expectation, call, "All elements in array passed to 'toPromise'");
                });
            }
        }
    },

    take: {
        callability: function(expectation) {
            if (expectation.expectedCallCountIsSet === false) {
                throw new Error("'take' cannot be used before number of expected calls is set.");
            }
        },

        args: function(expectation, beginCall, endCall) {
            validateExpectationCallArg(expectation, beginCall, "First parameter in method 'take'");

            if (endCall !== undefined) {
                validateExpectationCallArg(expectation, endCall, "Second parameter in method 'take'");
            }
        }
    }
};

var noClone = {
    args: function(args) {
        if (args.length !== 1) {
            throw new Error("'noClone' accepts exactly one parameter");
        }
    }
};

module.exports.base = base;
module.exports.expect = expect;
module.exports.mocks = mocks;
module.exports.expectation = expectation;
module.exports.noClone = noClone;
