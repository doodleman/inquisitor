"use strict";

function NoCloneWrapper(value) {
    this.value = value;
}

module.exports = NoCloneWrapper;
