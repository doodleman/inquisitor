"use strict";

var util = require("util");

var _ = require("lodash");

var Base = require("./Base");
var constants = require("./constants");
var utils = require("./utils");
var NoCloneWrapper = require("./NoCloneWrapper");

var any = constants.any;
var anyOrNone = constants.anyOrNone;
var anyOrNoneToEnd = constants.anyOrNoneToEnd;

var validate = require("./validate").expectation;

function Expectation(mock) {
    Base.call(this);

    this.mock = mock;

    this.expectedArgs = [anyOrNoneToEnd];
    this.expectedArgsIsSet = false;
    this.predicateContext = this;

    this.actualCallCount = 0;

    this.expectedMinCallCount = 0;
    this.expectedMaxCallCount = Infinity;

    this.expectedMaxCallCountIsSet = false;
    this.expectedMinCallCountIsSet = false;
    this.expectedCallCountIsSet = false;

    this.resolves = [];

    this.after = [];
}

util.inherits(Expectation, Base);

["toBeCalled", "times", "at", "with", "set", "and", "but", "which"]
    .forEach(function(chainName) {
        utils.createChain(Expectation.prototype, chainName, function() { return this; });
    });

Expectation.prototype.least = function(count) {
    validate.least.callability(this);
    validate.least.args(this, count);

    this.expectedMinCallCount = count;
    this.expectedMinCallCountIsSet = true;
    this.expectedCallCountIsSet = true;
    return this;
};

utils.createChain(Expectation.prototype, "unlimitedly", function() {
    return this.least(0);
});

Expectation.prototype.most = function(count) {
    validate.most.callability(this);
    validate.most.args(this, count);

    this.expectedMaxCallCount = count;
    this.expectedMaxCallCountIsSet = true;
    this.expectedCallCountIsSet = true;
    return this;
};

Expectation.prototype.between = function(from, to) {
    validate.between.callability(this);
    validate.between.args(this, from, to);

    return this.least(from).most(to);
};

Expectation.prototype.exactly = function(count) {
    validate.exactly.callability(this);
    validate.exactly.args(this, count);

    return this.between(count, count);
};

utils.createChain(Expectation.prototype, "never", function() {
    return this.exactly(0);
});

utils.createChain(Expectation.prototype, "once", function() {
    return this.exactly(1);
});

utils.createChain(Expectation.prototype, "twice", function() {
    return this.exactly(2);
});

utils.createChain(Expectation.prototype, "thrice", function() {
    return this.exactly(3);
});

Expectation.prototype.args = function() {
    var expectedArgs = _.toArray(arguments);

    validate.args.callability(this);
    validate.args.args(expectedArgs); // Validate arguments for method 'args'

    this.expectedArgs = expectedArgs.map(function(arg) {
        if (!_.includes([any, anyOrNone, anyOrNoneToEnd], arg)) {
            return _.cloneDeepWith(arg, function(value) {
                if (value instanceof NoCloneWrapper) {
                    return value.value;
                }
                if (!utils.isCloneable(value)) {
                    return value;
                }
            });
        }

        return arg;
    });

    this.expectedArgsIsSet = true;
    return this;
};

Expectation.prototype.argsWhich = function(predicate, context) {
    validate.argsWhich.callability(this);
    validate.argsWhich.args(predicate);

    this.expectedArgs = predicate;
    if (arguments.length > 1) {
        this.predicateContext = context;
    }

    this.expectedArgsIsSet = true;
    return this;
};

Expectation.prototype.in = function() {
    var args = _.toArray(arguments);

    validate.in.callability(this);
    validate.in.args(args);

    args.forEach(function(sequence) {
        sequence.extend(this, 1, this.expectedMinCallCount);
    }, this);

    return this;
};

Expectation.prototype.toPromise = function(calls) {
    validate.toPromise.callability(this);
    validate.toPromise.args(calls);

    if (Array.isArray(calls)) {
        return calls.map(function(call) {
            return utils.callToPromise(this, call);
        }, this);
    }

    return utils.callToPromise(this, (calls === undefined) ? this.expectedMinCallCount : calls);
};

Expectation.prototype.take = function(beginCall, endCall) {
    validate.take.callability(this);
    validate.take.args(this, beginCall, endCall);

    return new ExpectationTake(this, beginCall, (endCall === undefined) ? beginCall : endCall);
};

function ExpectationTake(expectation, beginCall, endCall) {
    this.expectation = expectation;
    this.beginCall = beginCall;
    this.endCall = endCall;
}

ExpectationTake.prototype.in = function() {
    var args = _.toArray(arguments);

    validate.in.callability(this.expectation);
    validate.in.args(args);

    args.forEach(function(sequence) {
        sequence.extend(this.expectation, this.beginCall, this.endCall);
    }, this);

    return this;
};

ExpectationTake.prototype.giveBack = function() {
    return this.expectation;
};

module.exports = Expectation;
