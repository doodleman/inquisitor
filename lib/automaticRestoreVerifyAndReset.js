"use strict";

var chalk = require("chalk");
var _ = require("lodash");

var ExpectationError = require("./ExpectationError");
var utils = require("./utils");

var state = require("./state");

// detect mocha and:
// 1. add automatic 'restore', 'verify' and 'reset'
// 2. add guard preventing mocking before tests start
if (typeof afterEach !== "undefined" && typeof before !== "undefined") {
    afterEach(function() {
        if (this.currentTest.state === "passed") {
            try {
                var unhandledError = _.find(state.expectationErrors, function(error) {
                    return !error.mock.noVerifyValue;
                });

                if (unhandledError !== undefined) {
                    utils.throwAsUnhandled(unhandledError);
                }

                state.registeredMocksToVerify.forEach(function(mock) {
                    if (!mock.noVerifyValue) {
                        mock.verify();
                    }
                });
            }
            catch (error) {
                if (error instanceof ExpectationError) {
                    // failing the current test
                    // todo implement it in a post-test when mocha provides support for them
                    this.currentTest.title += chalk.red(" (post test verification)");
                    this.test.error(error);
                }
                else {
                    throw error;
                }
            }
        }

        state.expectationErrors = [];

        state.registeredMocksToVerify = [];

        state.registeredMocksToRestore.forEach(function(mock) {
            mock.restore();
            state.registeredMocks.delete(mock.mockingFunction);
        });
        state.registeredMocksToRestore = [];

        state.registeredMocksToReset.forEach(function(mock) {
            mock.initialize();
        });
        state.registeredMocksToReset.clear();
    });

    before(function() {
        state.testsStarted = true;
    });
}
else {
    throw new Error("Inquisitor didn't detect mocha.");
}
