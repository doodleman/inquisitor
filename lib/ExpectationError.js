"use strict";

var util = require("util");
var _ = require("lodash");
var chalk = require("chalk");
var chai = require("chai");

var utils = require("./utils");
var defaultComparator = require("./defaultComparator");
var constants = require("./constants");

var codes = {
    NO_EXPECTATIONS: 1,
    UNEXPECTED_CALL: 2,
    CANNOT_MATCH: 3,
    UNFULFILLED_EXPECTATIONS: 4,

    // for a single expectation
    UNFULFILLED_EXPECTATION: 5,
    COUNT_NOT_SET: 6,
    NEVER_TO_BE_CALLED: 7,
    SEQUENCE_FAILED: 8,
    PREDICATE_FAILED: 9,
    WRONG_ARGUMENT: 10,
    ARGUMENTS_TOO_FEW: 11,
    ARGUMENTS_TOO_FEW_EXACTLY: 12,
    ARGUMENTS_TOO_MANY: 13,
    ARGUMENTS_TOO_MANY_EXACTLY: 14
};

function ExpectationError(message, props) {
    chai.AssertionError.call(this, message, props);
    this.name = "ExpectationError";
}

util.inherits(ExpectationError, chai.AssertionError);

function makeVerbalExpectedCallCount(expectation) {
    if (expectation.expectedMinCallCount === expectation.expectedMaxCallCount) {
        return chalk.cyan(expectation.expectedMinCallCount);
    }
    if (expectation.expectedMaxCallCount === Infinity) {
        return "at least " + chalk.cyan(expectation.expectedMinCallCount);
    }
    if (expectation.expectedMinCallCount === 0) {
        return "at most " + chalk.cyan(expectation.expectedMaxCallCount);
    }
    return "from " + chalk.cyan(expectation.expectedMinCallCount) + " to " + chalk.cyan(expectation.expectedMaxCallCount);
}

function makeVerbalReport(expectation, index, color) {
    var report = "       " + (index + 1) + ". ";
    if (expectation.expectedCallCountIsSet) {
        report += "Fulfilled " + chalk[color](expectation.actualCallCount)
            + " call(s) out of " + makeVerbalExpectedCallCount(expectation) + ". ";
    }
    return report + "Expectation created\n     " + chalk.dim.grey(expectation.shortStack) + "\n";
}

function makeVerbalReportMulti(expectations, header, color) {
    var report = "     " + chalk[color](expectations.length)
        + " " + header + "\n";

    expectations.forEach(function(expectation, index) {
        report += makeVerbalReport(expectation, index, color);
    });

    return report;
}

function makeExpectationsReport(mock) {
    if (mock.modeValue === "sequential") {
        var reportHeader = "fulfilled expectation(s) out of " + chalk.cyan(mock.expectations.length);
        return makeVerbalReportMulti(utils.getFulfilledExpectations(mock), reportHeader, "green");
    }
    else { // "greedy"
        var unspecifiedExpectations = utils.getUnspecifiedExpectations(mock);
        var unfulfilledExpectations = utils.getUnfulfilledExpectations(mock);
        var fulfilledButNotRepletedExpectations = utils.getFulfilledButNotRepletedExpectations(mock);
        var repletedExpectations = utils.getRepletedExpectations(mock);

        var report = "     Out of " + chalk.cyan(mock.expectations.length) + " expectation(s) there were:\n\n";

        if (unspecifiedExpectations.length > 0) {
            report += makeVerbalReportMulti(unspecifiedExpectations, "with unspecified calls count", "red");
        }

        if (unfulfilledExpectations.length > 0) {
            report += makeVerbalReportMulti(unfulfilledExpectations, "unfulfilled", "red");
        }

        if (fulfilledButNotRepletedExpectations.length > 0) {
            report += makeVerbalReportMulti(fulfilledButNotRepletedExpectations, "fulfilled which still could be matched", "cyan");
        }

        if (repletedExpectations.length > 0) {
            report += makeVerbalReportMulti(repletedExpectations, "fully fulfilled", "green");
        }

        return report;
    }
}

function makeArgumentsCountReport(expectedArgs, actualArgs, excess, limitPrefix) {
    var limit = (excess === "few") ? utils.getMinArgsCount(expectedArgs) : utils.getMaxArgsCount(expectedArgs);

    return "Called with too " + excess + " arguments. " +
        "Expected " + limitPrefix + chalk.green(limit) + " " +
        "but provided " + chalk.red(actualArgs.length) + ".";
}

function makeMessage(code, mock, expectation, params) {
    var message;

    var verbalMockName = utils.getVerbalMockName(mock);
    var verbalExpectedCallCount;
    var standardMessageBegin;

    if (expectation !== undefined) {
        verbalExpectedCallCount = makeVerbalExpectedCallCount(expectation);

        standardMessageBegin = "Failed expectation for " + verbalMockName +
            " on the " + chalk.cyan(expectation.actualCallCount + 1) +
            " call (out of " + verbalExpectedCallCount + "). ";
    }

    switch (code) {
        case codes.NO_EXPECTATIONS:
            message = verbalMockName + " was called before any expectations were set.\n";
            break;
        case codes.UNEXPECTED_CALL:
            message = "Unexpected call of " + verbalMockName + ". " +
                "Already fulfilled all " + chalk.cyan(mock.expectations.length) + " expectation(s).\n";
            break;
        case codes.CANNOT_MATCH:
            message = "Couldn't find any matching expectations for " + verbalMockName + ".\n";
            break;
        case codes.UNFULFILLED_EXPECTATIONS:
            message = "Unfulfilled expectation(s) for " + verbalMockName + ".\n";
            break;
        case codes.UNFULFILLED_EXPECTATION:
            message = "Expectation for " + verbalMockName + " was not fulfilled. " +
                "Expected to be called " + verbalExpectedCallCount + " time(s), but was called " +
                chalk.red(expectation.actualCallCount) + " time(s).\n";
            break;
        case codes.COUNT_NOT_SET:
            message = "Expectation for " + verbalMockName + " cannot be verified before number of expected calls is set.\n";
            break;
        case codes.NEVER_TO_BE_CALLED:
            message = "Expected " + verbalMockName + " never to be called but it was called.\n";
            break;
        case codes.SEQUENCE_FAILED:
            message = standardMessageBegin + "Expectation fulfilled too soon.\n";
            break;
        case codes.PREDICATE_FAILED:
            message = standardMessageBegin + "Predicate returned false.\n";
            break;
        case codes.WRONG_ARGUMENT:
            message = standardMessageBegin;
            if (utils.getComparator(expectation).fn === defaultComparator) {
                message += "Argument " + chalk.cyan(params.wrongArgIndex + 1)
                    + " (out of " + chalk.cyan(params.args.length) + ") didn't match expected.\n";
            }
            else {
                message += "Comparator returned false for argument " + chalk.cyan(params.wrongArgIndex + 1)
                    + " (out of " + chalk.cyan(params.args.length) + ")\n";
            }
            break;
        case codes.ARGUMENTS_TOO_FEW:
            message = standardMessageBegin + makeArgumentsCountReport(expectation.expectedArgs, params.args, "few", "at least ") + "\n";
            break;
        case codes.ARGUMENTS_TOO_FEW_EXACTLY:
            message = standardMessageBegin + makeArgumentsCountReport(expectation.expectedArgs, params.args, "few", "") + "\n";
            break;
        case codes.ARGUMENTS_TOO_MANY:
            message = standardMessageBegin + makeArgumentsCountReport(expectation.expectedArgs, params.args, "many", "at most ") + "\n";
            break;
        case codes.ARGUMENTS_TOO_MANY_EXACTLY:
            message = standardMessageBegin + makeArgumentsCountReport(expectation.expectedArgs, params.args, "many", "") + "\n";
            break;
    }

    if (!_.includes([codes.COUNT_NOT_SET, codes.UNFULFILLED_EXPECTATIONS, codes.WRONG_ARGUMENT], code)
        && params && params.args && params.args.length) {
        var args = util.inspect(params.args).replace(/\n/g, "\n     ");
        if (args.length > constants.maxLenOfArgsPrint) {
            args = args.substr(0, constants.maxLenOfArgsPrint) + "(...) ]";
        }
        message += "\n     Called with arguments: \n     " + args + "\n";
    }

    message += "\n     Mock created\n  " + chalk.dim.grey(mock.shortStack) + "\n";

    if (expectation !== undefined) {
        message += "\n     Expectation created\n  " + chalk.dim.grey(expectation.shortStack) + "\n";
    }

    if (!_.includes([codes.NO_EXPECTATIONS, codes.UNEXPECTED_CALL, codes.COUNT_NOT_SET, codes.NEVER_TO_BE_CALLED], code)) {
        message += "\n" + makeExpectationsReport(mock);
    }

    message += "\n     Mode: " + chalk.cyan(mock.modeValue) + "\n";

    return chalk.yellow(message);
}

ExpectationError.throw = function(code, mock, expectation, params) {
    var message = makeMessage(code, mock, expectation, params);

    var props = {
        code: code,
        mock: mock,
        expectation: expectation
    };

    if (code === codes.WRONG_ARGUMENT && utils.getComparator(expectation).fn === defaultComparator) {
        props = _.extend(props, {
            expected: expectation.expectedArgs[params.wrongArgIndex],
            actual: params.args[params.wrongArgIndex],
            showDiff: true
        });
    }

    var error = new this(message, props);
    error.stack = utils.removeInquisitorStackFrames(error.stack);
    error.unalteredMessage = error.message;
    error.unalteredStack = error.stack;
    throw error;
};

ExpectationError.codes = codes;

module.exports = ExpectationError;
