"use strict";

var util = require("util");

var Set = require("es6-set");
var _ = require("lodash");

var Base = require("./Base");
var ExpectationError = require("./ExpectationError");
var codes = ExpectationError.codes;
var utils = require("./utils");
var state = require("./state");
var validate = require("./validate").mocks;

function MockBase() {
    this.initialize();

    var that = this;
    this.mockingFunction = function() {
        var args = _.toArray(arguments);

        var expectation = validateCallWithCatch(that, args);
        return performCall(that, expectation, args, this);
    };
}

util.inherits(MockBase, Base);

function MethodMock(mockedObject, mockedMethodName) {
    MockBase.call(this);

    this.restored = false;

    this.mockedMethodIsOwn = _.has(mockedObject, mockedMethodName);

    this.mockedObject = this.callbackContext = mockedObject;
    this.mockedMethodName = mockedMethodName;

    this.originalMethod = mockedObject[mockedMethodName];
    mockedObject[mockedMethodName] = this.mockingFunction;
}

util.inherits(MethodMock, MockBase);

function NonMethodMock(name) {
    MockBase.call(this);

    this.mockedMethodName = name;

    this.defaultReturnValue = undefined;
    this.defaultReturnValueIsSet = false;

    this.defaultCallbackFunction = undefined;
    this.defaultCallbackContext = undefined;
    this.defaultCallbackContextIsSet = false;
}

util.inherits(NonMethodMock, MockBase);

MockBase.prototype.initialize = function() {
    Base.call(this);

    this.modeValue = "sequential";
    this.modeValueIsSet = false;

    this.expectations = [];
    this.nextExpectationIndex = 0;

    this.firstExpectationError = undefined;
    this.noVerifyValue = false;
};

MockBase.prototype.mode = function(mode) {
    validate.mode.callability(this);
    validate.mode.args(mode);

    this.modeValue = mode;
    this.modeValueIsSet = true;

    utils.addToReset(this);

    return this;
};

NonMethodMock.prototype.defaultReturn = function(returnValue) {
    validate.defaultReturn.callability(this);

    this.defaultReturnValue = returnValue;
    this.defaultReturnValueIsSet = true;

    return this;
};

NonMethodMock.prototype.byDefaultReturn = NonMethodMock.prototype.byDefaultReturns = NonMethodMock.prototype.defaultReturn;

NonMethodMock.prototype.defaultCallback = function(callbackFunction, callbackContext) {
    validate.defaultCallback.callability(this);
    validate.defaultCallback.args(this, callbackFunction, callbackContext);

    this.defaultCallbackFunction = callbackFunction;

    if (arguments.length > 1) {
        this.defaultCallbackContext = callbackContext;
        this.defaultCallbackContextIsSet = true;
    }

    return this;
};

NonMethodMock.prototype.byDefaultCall = NonMethodMock.prototype.byDefaultCalls = NonMethodMock.prototype.defaultCallback;

function findMatchingExpectationIndex(mock, args) {
    return _.findIndex(mock.expectations, function(expectation) {
        if (expectation.expectedCallCountIsSet === false) {
            ExpectationError.throw(codes.COUNT_NOT_SET, mock, expectation, {args: args});
        }
        return expectation.actualCallCount < expectation.expectedMaxCallCount
            && sequenceOk(expectation, expectation.actualCallCount + 1)
            && argsMatch(expectation, args) === true;
    });
}

function argsMatch(expectation, actualArgs) {
    var expectedArgs = expectation.expectedArgs;
    if (Array.isArray(expectedArgs)) {
        var minArgsCount = utils.getMinArgsCount(expectedArgs);
        var maxArgsCount = utils.getMaxArgsCount(expectedArgs);

        var exactlyCount = (minArgsCount === maxArgsCount);

        if (actualArgs.length < minArgsCount) {
            return {code: (exactlyCount ? codes.ARGUMENTS_TOO_FEW_EXACTLY : codes.ARGUMENTS_TOO_FEW)};
        }

        if (actualArgs.length > maxArgsCount) {
            return {code: (exactlyCount ? codes.ARGUMENTS_TOO_MANY_EXACTLY : codes.ARGUMENTS_TOO_MANY)};
        }

        var wrongArgIndex = utils.findWrongArgIndex(expectation, actualArgs, expectedArgs);
        return (wrongArgIndex === -1) ? true : {code: codes.WRONG_ARGUMENT, wrongArgIndex: wrongArgIndex};
    }
    else { // expectedArgs is function
        var result = expectedArgs.apply(expectation.predicateContext, actualArgs);
        if (typeof result !== "boolean") {
            throw new ExpectationError("Predicate set for expectation returned " + typeof result + " " + result + " instead of boolean");
        }
        return result ? true : {code: codes.PREDICATE_FAILED};
    }
}

function sequenceOk(expectation, call) {
    if (Array.isArray(expectation.after[call])) {
        return expectation.after[call].every(function(element) {
            return element.expectation.actualCallCount >= element.expectedCall;
        });
    }
    return true;
}

function resolve(expectation, call) {
    var resolves = expectation.resolves[call];
    if (Array.isArray(resolves)) {
        resolves.forEach(function(resolve) {
            resolve();
        });
    }
}

function validateCallSequentialMode(mock, currentExpectationIndex, args) {
    if (currentExpectationIndex >= mock.expectations.length) {
        ExpectationError.throw(codes.UNEXPECTED_CALL, mock, undefined, {args: args});
    }

    var currentExpectation = mock.expectations[currentExpectationIndex];
    if (currentExpectation.expectedCallCountIsSet === false) {
        ExpectationError.throw(codes.COUNT_NOT_SET, mock, currentExpectation, {args: args});
    }

    var argsMatchResult = argsMatch(currentExpectation, args);

    if ((argsMatchResult !== true) || !sequenceOk(currentExpectation, currentExpectation.actualCallCount + 1)) {

        if (currentExpectation.actualCallCount >= currentExpectation.expectedMinCallCount) {
            // Cannot bump this expectation's call count but it's minimum is fulfilled so try next expectation
            return validateCallSequentialMode(mock, currentExpectationIndex + 1, args);
        }

        if (argsMatchResult !== true) {
            if (argsMatchResult.code === codes.WRONG_ARGUMENT) {
                ExpectationError.throw(codes.WRONG_ARGUMENT, mock, currentExpectation, {args: args, wrongArgIndex: argsMatchResult.wrongArgIndex});
            }
            ExpectationError.throw(argsMatchResult.code, mock, currentExpectation, {args: args});
        }
        ExpectationError.throw(codes.SEQUENCE_FAILED, mock, currentExpectation, {args: args});
    }

    return currentExpectation;
}

function validateCallGreedyMode(mock, args) {
    if (utils.getRepletedExpectations(mock).length === mock.expectations.length) {
        ExpectationError.throw(codes.UNEXPECTED_CALL, mock, undefined, {args: args});
    }

    var foundMatchingExpectationIndex = findMatchingExpectationIndex(mock, args);

    if (foundMatchingExpectationIndex === -1) {
        ExpectationError.throw(codes.CANNOT_MATCH, mock, undefined, {args: args});
    }

    return mock.expectations[foundMatchingExpectationIndex];
}

function isNeverToBeCalled(mock) {
    return (_.isEmpty(mock.expectations) === false) && (mock.expectations[0].expectedMaxCallCount === 0);
}

function validateCall(mock, args) {
    if (_.isEmpty(mock.expectations)) {
        ExpectationError.throw(codes.NO_EXPECTATIONS, mock, undefined, {args: args});
    }

    if (isNeverToBeCalled(mock)) {
        ExpectationError.throw(codes.NEVER_TO_BE_CALLED, mock, mock.expectations[0], {args: args});
    }

    return (mock.modeValue === "sequential") ? validateCallSequentialMode(mock, mock.nextExpectationIndex, args) : validateCallGreedyMode(mock, args);
}

function validateCallWithCatch(mock, args) {
    try {
        return validateCall(mock, args);
    }
    catch (error) {
        if (error instanceof ExpectationError) {
            state.expectationErrors.push(error);

            if (error.mock.firstExpectationError === undefined) {
                error.mock.firstExpectationError = error;
                utils.addToReset(error.mock);
            }
        }
        throw error;
    }
}

function determineFinalReturn(expectation, args, originalContext) {
    var callback = utils.getCallback(expectation, originalContext);
    var callbackReturn;

    if (callback !== undefined) {
        callbackReturn = callback.fn.apply(callback.context, args);
    }

    if (utils.isReturnValueSet(expectation)) {
        return utils.getReturnValue(expectation);
    }
    if (callback !== undefined) {
        return callbackReturn;
    }

    return undefined;
}

function propagateExpectations(mock, currentExpectation) {
    ++currentExpectation.actualCallCount;
    if (mock.modeValue === "sequential" && currentExpectation.actualCallCount === currentExpectation.expectedMaxCallCount) {
        ++mock.nextExpectationIndex;
    }
}

function performCall(mock, expectation, args, originalContext) {
    propagateExpectations(mock, expectation);

    var finalReturn = determineFinalReturn(expectation, args, originalContext);

    resolve(expectation, expectation.actualCallCount);

    return finalReturn;
}

MockBase.prototype.verify = function() {
    if (this.firstExpectationError !== undefined) {
        utils.throwAsUnhandled(this.firstExpectationError);
    }

    var unspecifiedExpectations = utils.getUnspecifiedExpectations(this);
    if (!_.isEmpty(unspecifiedExpectations)) {
        ExpectationError.throw(codes.COUNT_NOT_SET, this, unspecifiedExpectations[0]);
    }

    var unfulfilledExpectations = utils.getUnfulfilledExpectations(this);
    if (!_.isEmpty(unfulfilledExpectations)) {
        if (this.modeValue === "sequential") {
            ExpectationError.throw(codes.UNFULFILLED_EXPECTATION, this, unfulfilledExpectations[0]);
        }
        else {
            ExpectationError.throw(codes.UNFULFILLED_EXPECTATIONS, this);
        }
    }

    return this;
};

MethodMock.prototype.restore = function() {
    if (this.mockedMethodIsOwn) {
        this.mockedObject[this.mockedMethodName] = this.originalMethod;
    }
    else {
        delete this.mockedObject[this.mockedMethodName];
    }

    this.restored = true;
    return this;
};

// this is only for testing purpose
MockBase.prototype.noVerify = function() {
    this.noVerifyValue = true;
    return this;
};

function isMocked(fn) {
    validate.isMocked.args(fn);

    return state.registeredMocks.has(fn);
}

function configure(mockedFunction) {
    validate.configure.args(mockedFunction);

    return state.registeredMocks.get(mockedFunction);
}

function createMockFunction(name) {
    validate.createMockFunction.args(name);

    var mock = new NonMethodMock(name);
    utils.saveStacks(mock);

    state.registeredMocks.set(mock.mockingFunction, mock);
    return mock.mockingFunction;
}

function createMockObject() {
    validate.createMockObject.args(_.toArray(arguments));

    var mockObject = {};

    _.toArray(arguments).forEach(function(methodName) {
        mockObject[methodName] = createMockFunction(methodName);
        utils.saveStacks(configure(mockObject[methodName]), 1); // overwrite stack
    });

    return mockObject;
}

function mock(mockedObject, mockedMethodName) {
    validate.mock.callability(mockedMethodName);
    validate.mock.args(mockedObject, mockedMethodName);

    var mock = new MethodMock(mockedObject, mockedMethodName);
    utils.saveStacks(mock);

    state.registeredMocks.set(mock.mockingFunction, mock);
    state.registeredMocksToRestore.push(mock);

    return mock;
}

function walkAndMock(obj, depth, mocks, visited) {
    visited.add(obj);
    if (depth !== 0) {
        _.functionsIn(obj).filter(function(methodName) {
            return !isMocked(obj[methodName]);
        }).forEach(function(methodName) {
            mocks.push(mock(obj, methodName));
        });

        _.keysIn(obj).filter(function(key) {
            return _.isObject(obj[key]) && !visited.has(obj[key]);
        }).forEach(function(key) {
            walkAndMock(obj[key], depth - 1, mocks, visited);
        });
    }
}

function Mockify(mockedObject, depth) {
    this.mocks = [];

    walkAndMock(mockedObject, depth, this.mocks, new Set());
}

_.functionsIn(MockBase.prototype)
    .forEach(function(methodName) {
        Mockify.prototype[methodName] = function() {
            var args = _.toArray(arguments);

            _.each(this.mocks, function(mock) {
                mock[methodName].apply(mock, args);
            });

            return this;
        };
    });

function mockify(mockedObject, depth) {
    validate.mockify.callability();
    validate.mockify.args(mockedObject, depth);

    return new Mockify(mockedObject, depth === undefined ? Infinity : depth);
}

["with", "set", "and", "but", "which"]
    .forEach(function(chainName) {
        utils.createChain(MockBase.prototype, chainName, function() { return this; });
        utils.createChain(Mockify.prototype, chainName, function() { return this; });
    });

module.exports.MockBase = MockBase;
module.exports.MethodMock = MethodMock;

module.exports.isMocked = isMocked;
module.exports.configure = configure;

module.exports.createMockFunction = createMockFunction;
module.exports.createMockObject = createMockObject;
module.exports.mock = mock;
module.exports.mockify = mockify;
