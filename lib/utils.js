"use strict";

var path = require("path");

var _ = require("lodash");
var Promise = require("bluebird");
var chalk = require("chalk");

var state = require("./state");
var constants = require("./constants");
var defaultComparator = require("./defaultComparator");

var any = constants.any;
var anyOrNone = constants.anyOrNone;
var anyOrNoneToEnd = constants.anyOrNoneToEnd;

// regexps that match stack frames not interesting for users
// and will be omitted in the stack trace
var hiddenStackRegexps = [
    // inquisitor stack frames, this clutters the output a lot during MT testing
    new RegExp(path.join("inquisitor", "lib").replace("\\", "\\\\"))
];

module.exports.removeInquisitorStackFrames = function(stack) {
    return stack.split("\n")
        .filter(function(line) {
            // leaving these lines so it's visible that it's an error created after test
            if (line.match(/automaticRestoreVerifyAndReset\.js/) !== null) {
                return true;
            }
            return hiddenStackRegexps
                .every(function(regexp) {
                    return line.match(regexp) === null;
                });
        }).join("\n");
};

module.exports.isInt = function(number) {
    return number === parseInt(number);
};

module.exports.isCloneable = function(value) {
    return !_.isError(value) && !_.isFunction(value) && !_.isElement(value) && !_.isWeakMap(value);
};

module.exports.getMinArgsCount = function(expectedArgs) {
    return _.findLastIndex(expectedArgs, function(arg) {
        return (arg !== anyOrNone && arg !== anyOrNoneToEnd);
    }) + 1;
};

module.exports.getMaxArgsCount = function(expectedArgs) {
    if (_.last(expectedArgs) === anyOrNoneToEnd) {
        return Infinity;
    }
    return expectedArgs.length;
};

module.exports.findWrongArgIndex = function(expectation, actualArgs, expectedArgs) {
    var comparator = module.exports.getComparator(expectation);

    return _.findIndex(actualArgs, function(actualArg, index) {
        var expectedArg = expectedArgs[index];
        if (index >= expectedArgs.length || _.includes([any, anyOrNone, anyOrNoneToEnd], expectedArg)) {
            return false;
        }

        var result = comparator.fn.call(comparator.context, actualArg, expectedArg, index);
        if (typeof result !== "boolean") {
            throw new Error("Comparator returned " + typeof result + " " + result + " instead of boolean.");
        }
        return !result;
    });
};

module.exports.callToPromise = function(expectation, call) {
    return new Promise(function(resolve) {
        if (Array.isArray(expectation.resolves[call]) === false) {
            expectation.resolves[call] = [];
        }
        expectation.resolves[call].push(resolve);
    });
};

module.exports.createChain = function(prototype, chainName, callback) {
    prototype["_" + chainName] = callback;

    Object.defineProperty(prototype, chainName, {
        enumerable: true,
        get: callback
    });
};

module.exports.getComparator = function(expectation) {
    var fromThisTake = _.find([expectation, expectation.mock], function(obj) {
        return obj.comparatorFunction !== undefined;
    });

    if (fromThisTake !== undefined) {
        return {
            fn: fromThisTake.comparatorFunction,
            context: fromThisTake.comparatorContextIsSet ? fromThisTake.comparatorContext : expectation
        };
    }

    return {
        fn: defaultComparator,
        context: expectation
    };
};

module.exports.getReturnValue = function(expectation) {
    if (expectation.returnValueIsSet) {
        return expectation.returnValue;
    }
    else if (expectation.mock.returnValueIsSet) {
        return expectation.mock.returnValue;
    }
    return expectation.mock.defaultReturnValue;
};

module.exports.isReturnValueSet = function(expectation) {
    return expectation.returnValueIsSet || expectation.mock.returnValueIsSet || expectation.mock.defaultReturnValueIsSet;
};

module.exports.getCallback = function(expectation, originalContext) {
    if (expectation.callbackFunction !== undefined) {
        return {
            fn: expectation.callbackFunction,
            context: expectation.callbackContextIsSet ? expectation.callbackContext : originalContext
        };
    }
    else if (expectation.mock.callbackFunction !== undefined) {
        return {
            fn: expectation.mock.callbackFunction,
            context: expectation.mock.callbackContextIsSet ? expectation.mock.callbackContext : originalContext
        };
    }
    else if (expectation.mock.defaultCallbackFunction !== undefined) {
        return {
            fn: expectation.mock.defaultCallbackFunction,
            context: expectation.mock.defaultCallbackContextIsSet ? expectation.mock.defaultCallbackContext : originalContext
        };
    }

    return undefined;
};

module.exports.getVerbalMockName = function(mock) {
    return (mock.mockedMethodName === undefined) ? "anonymous mock function" : chalk.cyan(mock.mockedMethodName);
};

module.exports.getUnfulfilledExpectations = function(mock) {
    return mock.expectations.filter(function(expectation) {
        return expectation.expectedCallCountIsSet
            && expectation.actualCallCount < expectation.expectedMinCallCount;
    });
};

module.exports.getFulfilledExpectations = function(mock) {
    return mock.expectations.filter(function(expectation) {
        return expectation.expectedCallCountIsSet
            && expectation.actualCallCount >= expectation.expectedMinCallCount;
    });
};

module.exports.getRepletedExpectations = function(mock) {
    return mock.expectations.filter(function(expectation) {
        return expectation.expectedCallCountIsSet
            && expectation.actualCallCount === expectation.expectedMaxCallCount;
    });
};

module.exports.getFulfilledButNotRepletedExpectations = function(mock) {
    return mock.expectations.filter(function(expectation) {
        return expectation.expectedCallCountIsSet
            && expectation.actualCallCount >= expectation.expectedMinCallCount
            && expectation.actualCallCount < expectation.expectedMaxCallCount;
    });
};

module.exports.getUnspecifiedExpectations = function(mock) {
    return mock.expectations.filter(function(expectation) {
        return expectation.expectedCallCountIsSet === false;
    });
};

module.exports.getMock = function(base) {
    return (base.mock !== undefined) ? base.mock : base;
};

module.exports.isMock = function(base) {
    return (base.mock === undefined);
};

module.exports.addToReset = function(mock) {
    if (mock.originalMethod === undefined) { // check if it's a NonMethodMock
        state.registeredMocksToReset.add(mock);
    }
};

module.exports.saveStacks = function(obj, offset) {
    if (offset === undefined) {
        offset = 0;
    }

    Error.captureStackTrace(obj);

    obj.stack = module.exports.removeInquisitorStackFrames(obj.stack);

    obj.shortStack = obj.stack.split("\n")[1 + offset];

    // if we don't do this, mocha will break
    obj.shortStack = obj.shortStack.replace(path.join(process.cwd(), "/"), "");

    return obj;
};

// as the stack was replaced with a simple string (it's no longer a getter) we need to update also the stack
function appendMessage(error, text) {
    var messageBegin = error.stack.indexOf(error.message);
    var messageEnd = messageBegin + error.message.length;
    error.message += text;
    var messageBefore = error.stack.slice(0, messageBegin);
    var messageAfter = error.stack.slice(messageEnd);
    error.stack = messageBefore + error.message + messageAfter;
}

module.exports.throwAsUnhandled = function(error) {
    error.stack = error.unalteredStack;
    error.message = error.unalteredMessage;

    appendMessage(error, chalk.yellow("\n     Rethrown"));

    var dummy = module.exports.saveStacks({});

    appendMessage(error, chalk.dim.grey("\n  " + dummy.shortStack + "\n"));

    appendMessage(error, chalk.red("\n     This error was not handled properly.\n" +
        "     It was probably caught without being rethrown or became an unhandled promise rejection so mocha couldn't handle it.\n" +
        "     Consider rewriting your code so it properly handles errors.\n" +
        "     As a last resort, if there is absolutely no possibility to handle this error, you can treat this message as an advice.\n"));

    throw error;
};
