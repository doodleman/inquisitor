# Inquisitor

This readme is divided into two main sections:

+ [Guide](#guide) - Most important topics divided into coherent sections. Good to start. Gradually advances to more difficult and obscure subjects.
+ [API](#api) - Regular API

## Guide

### Precondition

Add this at the top of the file.

```javascript
var inq = require("inquisitor");
```

### Mocking

Inquisitor is all about mocking functions and then setting expectations on them.

There are two main approaches in mocking with Inquisitor.

+ Mock an existing object's method. Such mocked function *lives* until the test case ends the original function is then restored.
+ Create *fresh* mock object or function which can be used in multiple test cases.

#### Mocking existing objects

It's the simplest approach to mocking.

Consider you have an object `myObject` with method `foo`.

Mocking without any additional configuration:

```javascript
inq.mock(myObject, "foo");
```

For example add a callback:

```javascript
inq.mock(myObject, "foo").with.callback(myFunction);
```

To mock all methods of an object use `mockify`.

```javascript
inq.mockify(myObject);
```

#### Creating mock objects and functions

If you don't want to alter any existing objects you can create *mock object*. Pass desired method names as parameters.

```javascript
var myMockObject = inq.createMockObject("foo", "bar");
```

`myMockObject` is now like this: {foo: *method*, bar: *method*}

Or create just a single *mock function*

```javascript
var myMockFunction = inq.createMockFunction();
```

### Expecting

Calling `inq.expect` with a mocked function as parameter creates a new expectation that can be parametrized with methods like `.exactly` and `.args`.

```javascript
inq.expect(myObject.foo).once.with.args(1, 2, 3);
```

### Configuring

Function `inq.configure` is especially useful if you created mocks with `inq.createMockObject` or `inq.createMockFunction`.
In fact `inq.configure` just returns the handler of the mocked function. 
`inq.mock` already returns such handler, but `inq.createMockObject` or `inq.createMockFunction` return new object or function.
So `inq.configure` comes in handy.

Example:

```javascript
var myMockObject = inq.createMockObject("foo", "bar"); 

describe("something", function() {
    it("should work", function() {
        inq.configure(myMockObject.foo).with.return(42); // this configuration stays only for this test case
        
        // ...
    });
});
```

Configuring mocked functions created with `inq.createMockObject` or `inq.createMockFunction` remains only until the test case ends.

The exceptions to this rule are functions [defaultReturn](#defaultreturn-bydefaultreturns) and [defaultCallback](#defaultcallback-bydefaultcalls).

Example:

```javascript
var myMockObject = inq.createMockObject("foo", "bar"); 

// Use defaultReturn before tests start
inq.configure(myMockObject.foo).with.defaultReturn(100);
inq.configure(myMockObject.bar).with.defaultReturn(200);

describe("something", function() {
    it("should work", function() {
        inq.configure(myMockObject.foo).with.return(42); // set return only for this test case
        
        // ...
    });
});
```

### Language Chains

Inquisitor follows BDD style just like [chai.js](http://chaijs.com/) thus it's advisable to use chains to improve the readability of creation mocks and expectations. They do not provide testing capabilities.

They are NO-OP.

#### Available language chains for mocking or configuring mocks:

+ with
+ set 
+ and 
+ but
+ which

Example: `inq.mock(myObject, "foo").which.calls(myCallback).but.returns(42)`

or: `inq.configure(myMockObject.foo).with.callback(myCallback).and.return(42)`

#### Available language chains for expectations's creation:

+ toBeCalled
+ times
+ at
+ with
+ set
+ and
+ but
+ which

Very verbose example: `inq.expect(myObject.foo).toBeCalled.at.least(7).times.with.args(1, 2, 3).which.compareWith(myComparator)`

### Automatic verifying and restoring

Verifying is what's done after each test to check if there are some mocks that haven't had all their expectations fulfilled. 
Verification can be triggered earlier. See [verify](#verify)

When mocking existing object's method the original method is replaced with a mocking function which registers each call and passed arguments.
When restored the original method is put back to its place.
It's done after each test and can be triggered earlier. See [restore](#restore).

### Modes

Right now two modes are available: "sequential" and "greedy". Default mode is "sequential". Set mode with function `mode`.

With `inq.mock`:

```javascript
inq.mock(myObject, "foo").with.mode("greedy");
```

With `inq.configure`:

```javascript
inq.configure(myMockObject.foo).with.mode("greedy");
```

Modes change how mocks fulfills their expectations.

#### Sequential mode - default

In the scope of a mock all expectations must be fulfilled in the same order as they were set.

#### Greedy mode

Inquisitor will try to look through all already set expectations and find the first matching one.

### Sequence

Sequence allows you to force different mocks to be synchronized. 
But can also be used in scope of a single mock in greedy mode.

#### How to use Sequence?

Consider you have two mocked functions `foo` and `bar`.

```javascript
var seq = new inq.Sequence;
inq.expect(foo).once.in(seq);
inq.expect(bar).once.in(seq);
```

Now fulfilling the expectations in a different order than the order in which the expectations were added to the sequence will make the test fail.

```javascript
bar();
foo();
// This will fail!
```

#### Take

Use `.take` to put only some calls of an expectation to a sequence.

```javascript
var seq = new inq.Sequence;
inq.expect(foo).exactly(5).times.and.take(4).in(seq);
inq.expect(bar).exactly(7).times.and.take(2,5).in(seq); // second call of this expectation must be after the fourth call of fooMock
inq.expect(baz).exactly(6).times.and.take(3).in(seq);  // third call of this expectation must be after the fifth call of barMock
```

### About ExpectationError

When something that makes an expectation fail happens there is thrown an instance of `ExpectationError`.

You can get it like that: `inq.ExpectationError`

Your code should NOT catch them. They should be left alone and be properly handled by mocha. 

### Mocking inherited methods

Mocking methods that are proprietary to the given object is straightforward: 
The original method is put aside and replaced with the mocking function. 
Later it's restored to its initial state by putting the original method back to its place. And everything stays as it was before.

Nevertheless when mocking methods that are inherited from the given object's prototype... things are handled differently.
First, we cannot replace that method with the mocking function because then all other inheritants would have it mocked which would led to problems.
Instead the mocking function is assigned directly to the object so it simply overrides the original inherited method.
It works AS IF the method was mocked but in fact it's obscured. 
On restoration the mocking function is simply detached (the key is deleted) from the object revealing back the original inherited method.

Mindfulness is advised when mocking normal methods and especially when mocking inherited ones as your code might 
do funny things involving prototypes which may cause unexpected behaviour or difficult to track bugs. 

## API

### Inquisitor's main instance

#### .mock

```javascript
.mock(mockedObject, mockedMethodName) -> Mock
```

+ *mockedObject* - Object that has method called `mockedMethodName`.
+ *mockedMethodName* - String with the name of the method to be mocked.

You can mock a method of an existing object. Using this function should be done inside of a test case.

#### .mockify

```javascript
.mockify(mockedObject, depth) -> Mockify
```

+ *mockedObject* - Object to be mocked
+ *depth* - Nothing or positive integer indicating how deeply traverse the object searching for methods to mock. Pass nothing to go infinitely deep.

Mocks all methods of an object. Returns special container object that has the same methods as class `Mock`. 
These methods will be applied to all mocked methods from the mocked object.
 
Example:

```javascript
inq.mockify(mockedObject).set.callback(console.log); // All methods from mockedObject will call console.log
```

#### .createMockObject

```javascript
.createMockObject([methodName1[, methodName2[, ...[, methodNameN]]]]) -> object
```

+ *methodNameN* - String with a name for method.

Creates entirely new mock object with methods with given names. Such object can be injected as a dependency of the system under test.
How it is going to be injected is up to you. You can for example use [proxyquire](https://www.npmjs.com/package/proxyquire).

#### .createMockFunction

```javascript
.createMockFunction([name]) -> function
```

+ *name* - Optional string with name used when reporting expectation failures.

Like `inq.createMockObject` but creates a single mocked function.

#### .configure

```javascript
.configure(mockedFunction) -> Mock
```

+ *mockedFunction* - Method that was previously mocked with `inq.mock` or comes from object created by `inq.createMockObject` or function created by `createMockFunction`

Access the handler object for this mocked function.

See [configuring](#configuring)

#### .expect

```javascript
.expect(mockedFunction) -> Expectation
```

+ *mockedFunction* - Method that was previously mocked with `inq.mock` or comes from object created by `inq.createMockObject` or function created by `createMockFunction`

Add a new expectation for this mocked function. The expectation should be further parametrized using expectation methods. 

#### .isMocked

```javascript
.isMocked(fn) -> Boolean
```

+ *fn* - Function which should be tested for being mocked.

Returns `true` if given function is mocked. `false` otherwise.

### Mock

#### .mode 

```javascript
.mode(mode) -> Mock
```

+ *mode* - String "greedy" or "sequential"

See [modes](#mode) for more information.

#### .restore 

(only available for mocks created by `inq.mock`)

```javascript
.restore() -> Mock
```

Restores the original method. `.restore` is called automatically after each test that uses mocks in Mocha, but you can call it earlier if you want.

#### .verify

```javascript
.verify() -> Mock
```

Verifies if all expectations were fulfilled. `.verify` is called automatically after each test that uses mocks in Mocha, but you can call it earlier if you want.

#### .defaultReturn / .byDefaultReturns

(only available for mocks created by `inq.createMockObject` and `inq.createMockFunction`)

```javascript
.defaultReturn(returnValue) -> Mock
```

+ *returnValue* - Value that the mocked function should return when fulfilling an expectation.

Sets return value that won't be reset on the end of a test, but gets overridden by normal `.return`

See [configuring](#configuring)

#### .defaultCallback / .byDefaultCalls

(only available for mocks created by `inq.createMockObject` and `inq.createMockFunction`)

```javascript
.defaultCallback(callbackFunction[, callbackContext]) -> Mock
```

+ *callbackFunction* - Function to be called on fulfilling of an expectation.
+ *callbackContext* - Object to which *callbackFunction* should be bound. If you don't pass any, the original context will be used.

Sets callback that won't be reset on the end of a test, but gets overridden by normal `.callback`

See [configuring](#configuring)

### Mock's and Expectation's shared methods

#### .comparator / .compareWith

```javascript
.comparator(comparatorFunction[, comparatorContext]) -> Mock / Expectation
```

+ *comparatorFunction* - Function returning boolean and taking three arguments:
    + *actualArg* - Actual argument.
    + *expectedArg* - Expected argument.
    + *index* - Zero-based current arguments' index.
+ *comparatorContext* - Object to which *comparatorFunction* should be bound. If you don't pass any, the expectation object will be used.

You can provide your own comparator that will be used for comparison between actual and expected arguments. By default `isEqual` from [lodash.js](https://lodash.com/docs#isEqual) is used.

If you set comparator for a mock, it will apply to all its expectations.

Setting comparator for an expectation takes precedence over the one set for the mock.

#### .return / .returns

```javascript
.return(returnValue) -> Mock / Expectation
```

+ *returnValue* - Value that the mocked function should return when fulfilling an expectation.
    
If you set return value for a mock, it will apply to all its expectations.
 
Setting return value for an expectation takes precedence over the one set for the mock.

#### .callback / .call / .calls

```javascript
.callback(callbackFunction[, callbackContext]) -> Mock / Expectation
```

+ *callbackFunction* - Function to be called on fulfilling of an expectation. Optionally pass string `"original"` to use original function.
+ *callbackContext* - Object to which *callbackFunction* should be bound. If you don't pass any, the original context will be used.

`"original"` can be only applied to mocks created by `inq.mock`.

If you set callback for a mock, it will apply to all its expectations.

Setting callback for an expectation takes precedence over the one set for the mock.

### Expectation

#### .exactly

```javascript
.exactly(count) -> Expectation
```

+ *count* - Non-negative integer indicating how many calls are to be expected.

#### .never

```javascript
.never -> Expectation
```

Syntactic sugar for `.exactly(0)`.

#### .once

```javascript
.once -> Expectation
```

Syntactic sugar for `.exactly(1)`.

#### .twice

```javascript
.twice -> Expectation
```

Syntactic sugar for `.exactly(2)`.

#### .thrice

```javascript
.thrice -> Expectation
```

Syntactic sugar for `.exactly(3)`.

#### .least

```javascript
.least(count) -> Expectation
```

+ *count* - Non-negative integer indicating how many calls are to be expected at least.

#### .unlimitedly

```javascript
.unlimitedly -> Expectation
```

Syntactic sugar for `.least(0)`.

#### .most

```javascript
.most(count) -> Expectation
```

+ *count* - Non-negative integer indicating how many calls are to be expected at most.

#### .between

```javascript
.between(from, to) -> Expectation
```

+ *from* - Non-negative integer indicating how many calls are to be expected at least.
+ *to* - Non-negative integer indicating how many calls are to be expected at most.

Shortcut for `least(from).most(to)`.

#### .args

```javascript
.args([value1[, value2[, ...[, valueN]]]]) -> Expectation
```

+ *valueN* - Anything you expect the mocked method to be called with as Nth parameter.

By default if you don't use `.args`, the expectation matches any arguments.

*Note:* Beware that there is a difference between those two:

```javascript
.args()
```

```javascript
.args(undefined)
```

##### Arguments cloning

Arguments are by default cloned with `cloneDeepWith` from [lodash.js](https://lodash.com/docs#cloneDeepWith)
with customizer which leaves uncloneable values uncloned instead of producing an empty object.

If you want to prevent an argument from being cloned, wrap it with `inq.noClone`.

Examples:

```javascript
.args(inq.noClone(myObj1), myObj2, inq.noClone(myObj3))
```

```javascript
// 'noClone' can also be used on properties
var expected = {
    a: {
        b: inq.noClone(myObj)
    }
};

...

.args(expected)
```

##### Special arguments

Use them instead of specific arguments.

+ `inq.any` - Matches anything but *not* no argument at all.
+ `inq.anyOrNone` - Matches anything or no argument (use as last arguments).
+ `inq.anyOrNoneToEnd` - Works like `inq.anyOrNone` repeated infinitely many times (must be last argument).

#### .argsWhich

```javascript
.argsWhich(predicate[, context]) -> Expectation
```

+ *predicate* - Function taking the same arguments as the actual call and returning boolean.
+ *context* - Object to which *predicate* should be bound. If you don't pass any, the expectation object will be used.

You can provide your own arguments validation.

#### .toPromise

```javascript
.toPromise([call | calls]) -> Promise / Array of Promises
```

+ *call* - Positive integer - to make a promise that will be fulfilled after that call.
+ *calls* - Array of positive integers - to make an array of promises that will be fulfilled after those calls.

Pass nothing to make a promise that will be fulfilled when expectation gets fulfilled.

#### .in
    
```javascript
.in(sequence1[, sequence2[, ...[, sequenceN]]]) -> Expectation
```
    
+ *sequenceN* - Instance of `inq.Sequence` class.
    
Puts expectation into one or more sequences.

#### .take

```javascript
.take(call) -> ExpectationTake
.take(beginCall, endCall) -> ExpectationTake
```

+ *call* - Positive integer - single call of the expectation.
+ *beginCall* - Positive integer - beginning call of the expectation.
+ *endCall* - Positive integer - ending call of the expectation.

`.take` encapsulates a single expected call or a subsequence of expected calls.

Provide one argument to encapsulate a single call of the expectation.

Provide two arguments `beginCall` and `endCall` to encapsulate calls from `beginCall` to `endCall` inclusively.

The resulting encapsulation can be put into a [sequence](#sequence). So far it's the only reason to use `.take`.

See the [guide](#take) for example how to use `.take`

### ExpectationTake

#### .in

```javascript
.in(sequence1[, sequence2[, ...[, sequenceN]]]) -> ExpectationTake
```
    
+ *sequenceN* - Instance of `inq.Sequence` class.

Puts an expectation take into one or more [sequences](#sequences).

#### .giveBack

```javascript
.giveBack() -> Expectation
```

The opposite of `.take`. Simple as that.
