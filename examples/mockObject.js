"use strict";

var inq = require("../");

var mockObject = inq.createMockObject("foo");

inq.configure(mockObject.foo).with.defaultReturn(42);

describe("example1", function() {
    it("case 1", function() {
        inq.configure(mockObject.foo).return(32);
        inq.expect(mockObject.foo).once.return(22);
        mockObject.foo();
    });

    it("case 2", function() {
        inq.expect(mockObject.foo).once;
        mockObject.foo();
    });
});
