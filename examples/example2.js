"use strict";

var inq = require("../");

var stub = {
    foo: function() {
    }
};

describe("example2", function() {
    it("should fail due to failed argument comparison - with very nice diff", function() {
        inq.mock(stub, "foo");

        inq.expect(stub.foo).toBeCalled.once.with.args({
            a: 1,
            b: 2,
            c: {
                a: 1,
                b: 2,
                c: 3
            }
        });

        stub.foo({
            c: {
                a: 1,
                b: "foo",
                c: 3
            },
            b: 2
        });
    });
});
