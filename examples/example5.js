"use strict";

var inq = require("../");

var stub = {
    foo: function() {
    }
};

describe("example5", function() {
    it("should pass - showing how any, anyOrNone and anyOrNoneToEnd work", function() {
        inq.mock(stub, "foo");

        inq.expect(stub.foo).once.with.args(inq.any);
        inq.expect(stub.foo).once.with.args(inq.anyOrNone);
        inq.expect(stub.foo).once.with.args("foo", inq.anyOrNoneToEnd);

        stub.foo(42);
        stub.foo();
        stub.foo("foo", 1, {}, "bar");
    });
});
