"use strict";

var inq = require("../");

var stub = {
    foo: function() {
    }
};

describe("example1", function() {
    it("should fail due to unfulfilled expectation", function() {
        inq.mock(stub, "foo");

        inq.expect(stub.foo).once;
    });
});
