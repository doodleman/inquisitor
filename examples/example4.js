"use strict";

var inq = require("../");

var stub = {
    foo: function() {
    }
};

describe("example4", function() {
    it("should fail with nice report of expectations in greedy mode", function() {
        inq.mock(stub, "foo").set.mode("greedy");

        inq.expect(stub.foo).thrice.with.args(1);
        inq.expect(stub.foo).once.with.args(2);
        inq.expect(stub.foo).twice.with.args(3);
        inq.expect(stub.foo).between(2, 5).times.with.args(4);

        stub.foo(4);
        stub.foo(2);
        stub.foo(4);
        stub.foo(1);
        stub.foo(4);
        stub.foo(5);
    });
});
