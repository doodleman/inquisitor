"use strict";

var inq = require("../");

var stub = {
    foo: function() {
    }
};

describe("example3", function() {
    it("should pass thanks to greedy mode", function() {
        inq.mock(stub, "foo").set.mode("greedy");

        inq.expect(stub.foo).once.with.args(1);
        inq.expect(stub.foo).twice.with.args(2);
        inq.expect(stub.foo).thrice.with.args(3);

        // Order of all expected calls doesn't matter in greedy mode
        stub.foo(3);
        stub.foo(3);
        stub.foo(1);
        stub.foo(2);
        stub.foo(3);
        stub.foo(2);
    });
});
