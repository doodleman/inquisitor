"use strict";

var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

describe("#return()", function() {
    forSuts(["mock", "expectation"], "return", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);
        if (fixture.sutName === "mock") {
            cases.throwsWhenCalledAfterAnyExpectationsWereSet(fixture);
        }
    });
});

describe("#defaultReturn()", function() {
    forSuts(["mock"], "defaultReturn", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);
        cases.throwsWhenCalledAfterAnyExpectationsWereSet(fixture);
    });
});
