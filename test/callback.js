"use strict";

var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

describe("#callback()", function() {
    forSuts(["mock", "expectation"], "callback", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);
        cases.throwsWhenCalledWithValueOtherThanFunction(fixture);
        if (fixture.sutName === "mock") {
            cases.throwsWhenCalledAfterAnyExpectationsWereSet(fixture);
        }
    });
});

describe("#defaultCallback()", function() {
    forSuts(["mock"], "defaultCallback", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);
        cases.throwsWhenCalledWithValueOtherThanFunction(fixture);
        cases.throwsWhenCalledAfterAnyExpectationsWereSet(fixture);
    });
});
