"use strict";

var chai = require("chai");

var inq = require("../");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");
var methods = require("../testutils/methods");

describe("#args()", function() {
    forSuts(["expectation"], "args", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);

        it("should throw if #argsWhich() was used before", function() {
            methods.callWithExampleArgs(fixture, "argsWhich");
            chai.expect(function() {
                methods.callWithExampleArgs(fixture);
            }).to.throw(Error, "Expected arguments cannot be set twice.");
        });

        it("should throw if previously set expectation call count to 0", function() {
            fixture.sut.never.toBeCalled;
            chai.expect(function() {
                methods.callWithExampleArgs(fixture);
            }).to.throw(Error, "'args' cannot be called along with 'never'/'exactly(0)/most(0)'.");
        });

        it("should throw if anyOrNoneToEnd is not the last parameter", function() {
            chai.expect(function() {
                fixture.sut.with.args(1, 3, inq.anyOrNoneToEnd, 4);
            }).to.throw(Error, "anyOrNoneToEnd can only be the last parameter in 'args'.");
        });

        it("should not throw if anyOrNoneToEnd is the last parameter", function() {
            chai.expect(function() {
                fixture.sut.with.args(1, 3, 4, inq.anyOrNoneToEnd);
            }).not.to.throw();
        });
    });
});

describe("#argsWhich()", function() {
    forSuts(["expectation"], "argsWhich", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);
        cases.throwsWhenCalledWithValueOtherThanFunction(fixture);

        it("should throw if #args() was used before", function() {
            fixture.sut.with.args();
            chai.expect(function() {
                methods.callWithExampleArgs(fixture);
            }).to.throw(Error, "Expected arguments cannot be set twice.");
        });

        it("should throw if previously set expectation call count to 0", function() {
            fixture.sut.never.toBeCalled;
            chai.expect(function() {
                methods.callWithExampleArgs(fixture);
            }).to.throw(Error, "'argsWhich' cannot be called along with 'never'/'exactly(0)/most(0)'.");
        });
    });
});
