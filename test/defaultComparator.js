"use strict";

var chai = require("chai");

var inq = require("../");

var sut = require("../lib/defaultComparator");

describe("defaultComparator()", function() {
    it("should correctly compare equivalent buffers", function() {
        var buffer1 = new Buffer([1, 2, 3, 4, 5]);
        var buffer2 = new Buffer([1, 2, 3, 4, 5]);

        chai.expect(sut(buffer1, buffer2)).to.be.true;
    });

    it("qwe", function() {
        chai.expect(sut({a: inq.any}, {a: inq.anyOrNone})).to.be.true;
    });

    it("should not modify 'expected' parameter even when placeholders were used", function() {
        var actual = {a: 5};
        var expected = {a: inq.any};
        Object.freeze(expected);
        sut(actual, expected);
    });
});
