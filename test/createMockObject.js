"use strict";

var chai = require("chai");

var sut = require("../");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

describe("#createMockObject()", function() {
    forSuts(["inquisitor"], "createMockObject", function(fixture) {
        cases.standard(fixture);

        it("should throw if called not with strings only", function() {
            chai.expect(function() {
                sut.createMockObject("foo", "bar", 42, "baz");
            }).to.throw(TypeError, "'createMockObject' expects only strings as parameters.");
        });

        it("should return an object", function() {
            chai.expect(sut.createMockObject()).to.be.an("object");
        });
    });
});
