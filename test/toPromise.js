"use strict";

var chai = require("chai");

var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");
var isPromise = require("is-promise");

describe("#toPromise()", function() {
    forSuts(["expectation"], "toPromise", function(fixture) {
        cases.exists(fixture);
        cases.throwsIfCallCountWasNotSet(fixture);

        it("should return a promise", function() {
            fixture.sut.toBeCalled.once;
            chai.expect(isPromise(fixture.sut.toPromise())).to.equal(true);
        });

        it("should return a promise if passed a number", function() {
            fixture.sut.toBeCalled.thrice;
            chai.expect(isPromise(fixture.sut.toPromise(2))).to.equal(true);
        });

        it("should return an array of promises if passed an array", function() {
            fixture.sut.toBeCalled.exactly(5);
            var promises = fixture.sut.toPromise([4, 5, 2]);

            chai.expect(promises).to.be.an.instanceOf(Array);
            promises.forEach(function(promise) {
                chai.expect(isPromise(promise)).to.equal(true);
            });
        });
    });
});
