"use strict";

var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

describe("#comparator()", function() {
    forSuts(["mock", "expectation"], "comparator", function(fixture) {
        cases.standard(fixture);
        cases.throwsWhenCalledTwice(fixture);
        cases.throwsWhenCalledWithValueOtherThanFunction(fixture);
        if (fixture.sutName === "mock") {
            cases.throwsWhenCalledAfterAnyExpectationsWereSet(fixture);
        }
    });
});
