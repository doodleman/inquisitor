"use strict";

var chai = require("chai");

var sut = require("../");

describe("inquisitor", function() {
    ["any", "anyOrNone", "anyOrNoneToEnd"].forEach(function(propertyName) {
        it("should have property '" + propertyName + "' that is an empty object", function() {
            chai.expect(sut).to.have.property(propertyName)
                .that.is.an("object")
                .and.deep.equal({});
        });
    });
});
