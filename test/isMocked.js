"use strict";

var chai = require("chai");

var sut = require("../");
var notBrokenTypeof = require("../testutils/notBrokenTypeof");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");
var stub = require("../testutils/stub");

describe("#isMocked()", function() {
    forSuts(["inquisitor"], "isMocked", function(fixture) {
        cases.standard(fixture);

        [null, true, 42, "foo", {}].forEach(function(value) {
            it("should throw if called with " + notBrokenTypeof(value), function() {
                chai.expect(function() {
                    sut.isMocked(value);
                }).to.throw(TypeError, "'isMocked' expects function as parameter");
            });
        });

        it("should return 'false' when called with an unmocked function", function() {
            chai.expect(sut.isMocked(function() {}))
                .to.equal(false);
        });

        it("should return 'false' when called with unmocked function and there was a mock function made before ", function() {
            sut.createMockFunction();

            chai.expect(sut.isMocked(function() {}))
                .to.equal(false);
        });

        it("should return 'false' when called with an unmocked function and there was some method mocked before ", function() {
            sut.mock(stub, "ownMethod");

            chai.expect(sut.isMocked(function() {}))
                .to.equal(false);
        });

        it("should return 'true' when called with a mocked method", function() {
            sut.mock(stub, "ownMethod");

            chai.expect(sut.isMocked(stub.ownMethod))
                .to.equal(true);
        });


        it("should return 'true' when called with a function created with 'createMockFunction'", function() {
            var mock = sut.createMockFunction();

            chai.expect(sut.isMocked(mock))
                .to.equal(true);
        });

        it("should return 'true' when called with a mocked method and there was a function created with 'createMockFunction' before", function() {
            sut.createMockFunction();
            sut.mock(stub, "ownMethod");

            chai.expect(sut.isMocked(stub.ownMethod))
                .to.equal(true);
        });

        it("should return 'true' when called with a function created with 'createMockFunction' and there was a method mocked before ", function() {
            sut.mock(stub, "ownMethod");
            var mock = sut.createMockFunction();

            chai.expect(sut.isMocked(mock))
                .to.equal(true);
        });
    });
});
