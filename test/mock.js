"use strict";

var chai = require("chai");

var sut = require("../");
var notBrokenTypeof = require("../testutils/notBrokenTypeof");
var stub = require("../testutils/stub");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

describe("#mock()", function() {
    forSuts(["inquisitor"], "mock", function(fixture) {
        cases.standard(fixture);
        cases.throwsIfTestsDidNotStart(fixture, /Cannot mock '\w+'\. Don't mock before tests start\./);

        it("should throw if mocking an already mocked method", function() {
            sut.mock(stub, "ownMethod");

            chai.expect(function() {
                sut.mock(stub, "ownMethod");
            }).to.throw(Error, "Cannot mock 'ownMethod'. Method was already mocked in this test case.");
        });

        [undefined, null, true, 42, "foo"].forEach(function(value) {
            it("should throw if called with " + notBrokenTypeof(value) + " as first parameter", function() {
                chai.expect(function() {
                    sut.mock(value);
                }).to.throw(TypeError, "'mock' expects object as first parameter.");
            });
        });

        [undefined, null, true, 42, {}].forEach(function(value) {
            it("should throw if called with " + notBrokenTypeof(value) + " as second parameter", function() {
                chai.expect(function() {
                    sut.mock({}, value);
                }).to.throw(TypeError, "'mock' expects string as second parameter.");
            });
        });

        it("should throw if called with non-existent method name as second parameter", function() {
            chai.expect(function() {
                sut.mock({}, "nonExistentMethod");
            }).to.throw(ReferenceError, "Given object has no method 'nonExistentMethod'.");
        });

        it("should throw if called with non-function property name as second parameter", function() {
            chai.expect(function() {
                sut.mock(stub, "ownNonFunctionProperty");
            }).to.throw(TypeError, "Property 'ownNonFunctionProperty' of given object isn't function.");
        });

        it("should not throw if called with mocked object's inherited method as second parameter", function() {
            chai.expect(function() {
                sut.mock(stub, "inheritedMethod");
            }).not.to.throw();
        });

        it("should leave inherited method intact if mocked", function() {
            var originalFunction = stub.inheritedMethod;
            sut.mock(stub, "inheritedMethod");

            chai.expect(stub.constructor.prototype.inheritedMethod).to.equal(originalFunction);
        });
    });
});
