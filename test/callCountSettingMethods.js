"use strict";

var _ = require("lodash");

var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");
var methods = require("../testutils/methods");

_.filter(methods.expectation, function(method) {
    return method.setsCount;
}).forEach(function(method) {
    describe("#" + method.name + "()", function() {
        forSuts(["expectation"], method.name, function(fixture) {
            cases.standard(fixture);
            if (methods.setsMinCallsCount(fixture.mutName)) {
                cases.throwsIfMinCallCountWasSet(fixture);
            }

            if (methods.setsMaxCallsCount(fixture.mutName)) {
                cases.throwsIfMaxCallCountWasSet(fixture);
            }
        });
    });
});
