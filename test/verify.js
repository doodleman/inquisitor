"use strict";

var chai = require("chai").use(require("chai-as-promised"));
var _ = require("lodash");
var Promise = require("bluebird");

var inq = require("../");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");
var methods = require("../testutils/methods");

var unhandledErrorMessageSuffix = "\n     This error was not handled properly.\n" +
    "     It was probably caught without being rethrown or became an unhandled promise rejection so mocha couldn't handle it.\n" +
    "     Consider rewriting your code so it properly handles errors.\n" +
    "     As a last resort, if there is absolutely no possibility to handle this error, you can treat this message as an advice.\n";

describe("#verify()", function() {
    forSuts(["mock"], "verify", function(fixture) {
        cases.standard(fixture);

        it("should not throw if no expectations were set", function() {
            var mock = inq.configure(inq.createMockFunction());
            chai.expect(function() {
                mock.verify();
            }).not.to.throw();
        });

        _.filter(methods.expectation, methods.setsCallsCount).forEach(function(methodSettingCallsCount) {
            it("should not throw if expectation set with '" + methodSettingCallsCount.name + "' was fulfilled", function() {
                var expectation = inq.expect(fixture.mockFunction);
                expectation[methodSettingCallsCount.name]
                    .apply(expectation, methodSettingCallsCount.args);

                _.times(methodSettingCallsCount.minCount || 0, fixture.mockFunction);

                chai.expect(function() {
                    fixture.sut.verify();
                }).not.to.throw();
            });
        });

        _.filter(methods.expectation, methods.setsPositiveMinCallsCount).forEach(function(methodSettingMinCallsCount) {
            it("should throw if expectation set with '" + methodSettingMinCallsCount.name + "' wasn't fulfilled", function() {
                var expectation = inq.expect(fixture.mockFunction);
                expectation[methodSettingMinCallsCount.name]
                    .apply(expectation, methodSettingMinCallsCount.args);

                chai.expect(function() {
                    fixture.sut.verify();
                }).to.throw(inq.ExpectationError);
            });
        });

        it("should throw even if the expectation error from the mock function was caught", function() {
            inq.expect(fixture.mockFunction).never;

            try {
                fixture.mockFunction();
            }
            catch (e) {} //eslint-disable-line no-empty

            chai.expect(function() {
                methods.callWithExampleArgs(fixture);
            }).to.throw(inq.ExpectationError, unhandledErrorMessageSuffix);
        });

        it("should throw even if the expectation error from the mock function became an unhandled rejection", function() {
            inq.expect(fixture.mockFunction).never;

            var promise = Promise.resolve()
                .then(function() {
                    fixture.mockFunction();
                })
                .finally(function() {
                    methods.callWithExampleArgs(fixture);
                });

            return chai.expect(promise).to.be.rejectedWith(inq.ExpectationError, unhandledErrorMessageSuffix);
        });
    });

});
