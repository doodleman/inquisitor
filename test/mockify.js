"use strict";

var chai = require("chai");

var sut = require("../");
var notBrokenTypeof = require("../testutils/notBrokenTypeof");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

var deepStub = {
    foo: {
        bar: {
            baz: function() {}
        }
    }
};

var loopedStub = {};
loopedStub.loop = loopedStub;

describe("#mockify()", function() {
    forSuts(["inquisitor"], "mockify", function(fixture) {
        cases.standard(fixture);
        cases.throwsIfTestsDidNotStart(fixture, "Cannot mockify object. Don't mock before tests start.");

        [undefined, null, true, 42, "foo"].forEach(function(value) {
            it("should throw if called with " + notBrokenTypeof(value) + " as first parameter", function() {
                chai.expect(function() {
                    sut.mockify(value);
                }).to.throw(TypeError, "'mockify' expects object as first parameter.");
            });
        });

        [null, true, "foo", {}].forEach(function(value) {
            it("should throw if called with " + notBrokenTypeof(value) + " as second parameter", function() {
                chai.expect(function() {
                    sut.mockify({}, value);
                }).to.throw(TypeError, "Depth' parameter must be number.");
            });
        });

        it("should mockify deeply", function() {
            sut.mockify(deepStub);

            chai.expect(sut.isMocked(deepStub.foo.bar.baz)).to.be.true;
        });

        it("should mockify to given depth", function() {
            sut.mockify(deepStub, 2);

            chai.expect(sut.isMocked(deepStub.foo.bar.baz)).to.be.false;
        });

        it("should not loop", function() {
            sut.mockify(loopedStub);
        });
    });
});
