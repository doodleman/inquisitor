"use strict";

var chai = require("chai");

var sut = require("../");
var notBrokenTypeof = require("../testutils/notBrokenTypeof");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");

describe("#createMockFunction()", function() {
    forSuts(["inquisitor"], "createMockFunction", function(fixture) {
        cases.standard(fixture);

        [{}, null, true, 42].forEach(function(value) {
            it("should throw if called with " + notBrokenTypeof(value) + " as first parameter", function() {
                chai.expect(function() {
                    sut.createMockFunction(value);
                }).to.throw(TypeError, "'createMockFunction' expects string or nothing as parameter.");
            });
        });

        it("should return a mocked function", function() {
            chai.expect(sut.createMockFunction())
                .to.be.a("function")
                .and.satisfy(sut.isMocked);
        });
    });
});
