"use strict";

var chai = require("chai").use(require("chai-as-promised"));
var sinon = require("sinon");

var inq = require("../");
var stub = require("../testutils/stub");

function setCallbackAndExpect(mock, callbackType, callback, context) {
    if (callbackType === "expectation-level callback") {
        if (arguments.length > 3) {
            return inq.expect(mock).set.callback(callback, context);
        }
        return inq.expect(mock).set.callback(callback);
    }
    if (callbackType === "mock-level callback") {
        if (arguments.length > 3) {
            inq.configure(mock).set.callback(callback, context);
        }
        else {
            inq.configure(mock).set.callback(callback);
        }
    }
    else {
        if (arguments.length > 3) {
            inq.configure(mock).set.defaultCallback(callback, context);
        }
        else {
            inq.configure(mock).set.defaultCallback(callback);
        }
    }

    return inq.expect(mock);
}

function setReturnAndExpect(mock, returnType, returnStub) {
    if (returnType === "expectation-level return") {
        return inq.expect(mock).set.return(returnStub);
    }
    if (returnType === "mock-level return") {
        inq.configure(mock).set.return(returnStub);
    }
    else {
        inq.configure(mock).set.defaultReturn(returnStub);
    }

    return inq.expect(mock);
}

function setComparatorAndExpect(mock, comparatorType, comparator, context) {
    if (comparatorType === "expectation-level return") {
        if (arguments.length > 3) {
            return inq.expect(mock).set.comparator(comparator, context);
        }
        return inq.expect(mock).set.comparator(comparator);
    }

    if (arguments.length > 3) {
        inq.configure(mock).set.comparator(comparator, context);
    }
    else {
        inq.configure(mock).set.comparator(comparator);
    }

    return inq.expect(mock);
}

describe("mock function", function() {
    var sut;

    var callbackContextStub = "callback context stub";
    var originalCallbackContextStub = "original context stub";
    var defaultCallbackContextStub = "default callback context stub";
    var mockCallbackContextStub = "mock-level callback context stub";
    var expectationCallbackContextStub = "expectation-level callback context stub";

    var comparatorContextStub = "comparator context stub";
    var mockComparatorContextStub = "mock-level comparator context stub";
    var expectationComparatorContextStub = "expectation-level comparator context stub";

    var predicateContextStub = "predicate context stub";

    var callbackMock;
    var defaultCallbackMock;
    var mockCallbackMock;
    var expectationCallbackMock;

    var comparatorMock;
    var mockComparatorMock;
    var expectationComparatorMock;

    var predicateMock;

    beforeEach(function() {
        sut = inq.createMockFunction("test mock");
        callbackMock = sinon.expectation.create("callback mock");
        defaultCallbackMock = sinon.expectation.create("default callback mock");
        mockCallbackMock = sinon.expectation.create("mock-level callback mock");
        expectationCallbackMock = sinon.expectation.create("expectation-level callback mock");

        comparatorMock = sinon.expectation.create("comparator mock");
        mockComparatorMock = sinon.expectation.create("mock-level comparator mock");
        expectationComparatorMock = sinon.expectation.create("expectation-level comparator mock");

        predicateMock = sinon.expectation.create("predicate mock");
    });

    it("should throw if called before any expectations were set", function() {
        inq.configure(sut).noVerify();

        chai.expect(function() {
            sut();
        }).to.throw(Error, /ExpectationError: .*test mock.* was called before any expectations were set\./);
    });

    it("should shorten the argument list, if they are too long", function() {
        inq.configure(sut).noVerify();

        // output consisting of numbers and 3 dots at the end
        var expectedErrorMsg = /[0-9\s]+\([.]{3}\) \]/;

        function action() {
            var array = [];
            for (var i = 0; i < 50; i++) {
                array.push(i);
            }
            sut(array);
        }

        chai.expect(action).to.throw(Error, expectedErrorMsg);
    });

    it("should throw with diff if called with arguments different than expected", function() {
        inq.configure(sut).noVerify();
        inq.expect(sut).once.with.args("correct");

        chai.expect(function() {
            sut("wrong");
        }).to.throw(inq.ExpectationError);

        try {
            sut("wrong");
        }
        catch (error) {
            if (error instanceof inq.ExpectationError) {
                chai.expect(error.showDiff).to.be.true;
                chai.expect(error.expected).to.equal("correct");
                chai.expect(error.actual).to.equal("wrong");
            }
            else {
                throw error;
            }
        }
    });

    it("should call the predicate set with #argsWhich() with given context and arguments passed", function() {
        var actualArgs = [1, 2, 3];

        predicateMock.once()
            .on(predicateContextStub)
            .withExactArgs.apply(predicateMock, actualArgs)
            .returns(true);

        inq.expect(sut).once.with.argsWhich(predicateMock, predicateContextStub);

        sut.apply(undefined, actualArgs);
        predicateMock.verify();
    });

    it("should throw if the predicate set with #argsWhich() returns false", function() {
        inq.configure(sut).noVerify();
        inq.expect(sut).once.with.argsWhich(function() {
            return false;
        });

        chai.expect(function() {
            sut();
        }).to.throw(inq.ExpectationError);
    });

    it("should deep-clone expected arguments", function() {
        var expectedArg = {};
        var actualArg = {};

        inq.expect(sut).once.with.args(expectedArg);

        expectedArg.change = 42; // if expected arg is not copied it will cause fail

        chai.expect(function() {
            sut(actualArg);
        }).not.to.throw();
    });

    it("should not clone expected argument if used 'noClone'", function() {
        var expectedArg = {};
        var actualArg = {prop: 42};

        inq.expect(sut).once.with.args(inq.noClone(expectedArg));

        expectedArg.prop = 42;

        chai.expect(function() {
            sut(actualArg);
        }).not.to.throw();
    });

    it("should correctly compare equivalent buffers", function() {
        var buffer1 = new Buffer([1, 2, 3, 4, 5]);
        var buffer2 = new Buffer([1, 2, 3, 4, 5]);

        inq.expect(sut).once.with.args(buffer1);

        chai.expect(function() {
            sut(buffer2);
        }).not.to.throw();
    });

    it("should return 'undefined' if no callback or return is set", function() {
        inq.expect(sut).once;

        chai.expect(sut()).to.equal(undefined);
    });

    ["default callback", "mock-level callback", "expectation-level callback"].forEach(function(callbackType) {
        it("should run set " + callbackType + " with given context and arguments passed, and return what callback returned", function() {
            var actualArgs = [1, 2, 3];
            var returnStub = "return stub";

            callbackMock.once()
                .on(callbackContextStub)
                .withExactArgs.apply(callbackMock, actualArgs)
                .returns(returnStub);

            setCallbackAndExpect(sut, callbackType, callbackMock, callbackContextStub).once;

            chai.expect(sut.apply(undefined, actualArgs)).to.equal(returnStub);
            callbackMock.verify();
        });

        it("should run " + callbackType + " with original context as default", function() {
            callbackMock.once()
                .on(originalCallbackContextStub);

            setCallbackAndExpect(sut, callbackType, callbackMock).once;

            sut.call(originalCallbackContextStub);
            callbackMock.verify();
        });
    });

    it("should not run default callback but expectation-level callback if both were set", function() {
        defaultCallbackMock.never();
        expectationCallbackMock.once().on(expectationCallbackContextStub);

        inq.configure(sut).set.defaultCallback(defaultCallbackMock, defaultCallbackContextStub);
        inq.expect(sut).once.with.callback(expectationCallbackMock, expectationCallbackContextStub);

        sut();

        defaultCallbackMock.verify();
        expectationCallbackMock.verify();
    });

    it("should not run mock-level callback but expectation-level callback if both were set", function() {
        mockCallbackMock.never();
        expectationCallbackMock.once().on(expectationCallbackContextStub);

        inq.configure(sut).set.callback(mockCallbackMock, mockCallbackContextStub);
        inq.expect(sut).once.with.callback(expectationCallbackMock, expectationCallbackContextStub);

        sut();

        mockCallbackMock.verify();
        expectationCallbackMock.verify();
    });

    it("should not run default callback but mock-level callback if both were set", function() {
        defaultCallbackMock.never();
        mockCallbackMock.once().on(mockCallbackContextStub);

        inq.configure(sut)
            .set.defaultCallback(defaultCallbackMock, defaultCallbackContextStub)
            .set.callback(mockCallbackMock, mockCallbackContextStub);
        inq.expect(sut).once;

        sut();

        defaultCallbackMock.verify();
        mockCallbackMock.verify();
    });

    it("should fulfill the expectation before running callback", function() {
        inq.expect(sut).once.with.callback(function() {
            inq.configure(sut).verify();
        });

        chai.expect(function() {
            sut();
        }).not.to.throw();
    });

    it("should run original function as callback if passed string 'original' to #callback() and if an existing object's method was mocked", function() {
        var originalCallbackMock = sinon.mock(stub);
        originalCallbackMock.expects("ownMethod").once();

        inq.mock(stub, "ownMethod");
        inq.expect(stub.ownMethod).once.with.callback("original");

        stub.ownMethod();

        originalCallbackMock.verify();
        originalCallbackMock.restore();
    });

    ["default return", "mock-level return", "expectation-level return"].forEach(function(returnType) {
        it("should return set " + returnType + " value", function() {
            var returnStub = "return stub";

            setReturnAndExpect(sut, returnType, returnStub).once;

            chai.expect(sut()).to.equal(returnStub);
        });
    });

    it("should return the value set by 'return' not the value from callback if both were set", function() {
        var returnStub = "return stub";
        var callbackReturnStub = "callback return stub";
        callbackMock.once().returns(callbackReturnStub);

        inq.expect(sut).once.with.callback(callbackMock).and.return(returnStub);

        chai.expect(sut()).to.equal(returnStub);

        callbackMock.verify();
    });

    ["mock-level comparator", "expectation-level comparator"].forEach(function(comparatorType) {
        it("should run set " + comparatorType + " with given context and correct arguments", function() {
            var actualArg = "actual arg";
            var expectedArg = "expected arg";

            comparatorMock.once()
                .on(comparatorContextStub)
                .withExactArgs(actualArg, expectedArg, 0)
                .returns(true);

            var expectation = setComparatorAndExpect(sut, comparatorType, comparatorMock, comparatorContextStub);
            expectation.once.with.args(expectedArg);

            sut(actualArg);
            comparatorMock.verify();
        });

        it("should run " + comparatorType + " with expectation as default context", function() {
            var expectation = setComparatorAndExpect(sut, comparatorType, comparatorMock).once.with.args("dummyArg");

            comparatorMock.once()
                .on(expectation)
                .returns(true);

            sut("dummyArg");
            comparatorMock.verify();
        });
    });

    it("should not run mock-level comparator but expectation-level comparator if both were set", function() {
        mockComparatorMock.never();
        expectationComparatorMock.once()
            .on(expectationComparatorContextStub)
            .returns(true);

        inq.configure(sut).set.comparator(mockComparatorMock, mockComparatorContextStub);
        inq.expect(sut)
            .once
            .with.comparator(expectationComparatorMock, expectationComparatorContextStub)
            .with.args("dummyArg");

        sut("dummyArg");

        mockComparatorMock.verify();
        expectationComparatorMock.verify();
    });

    it("should use set comparator to actually compare arguments", function() {
        inq.configure(sut).noVerify();
        inq.expect(sut).set.comparator(function(actual, expected) {
            return actual + expected === 10;
        }).twice.with.args(1, 2, 3);

        chai.expect(function() {
            sut(9, 8, 7);
        }).not.to.throw();

        chai.expect(function() {
            sut(9, 8, 6);
        }).to.throw(inq.ExpectationError);
    });

    it("should fulfill the promise created by #toPromise()", function() {
        var promise = inq.expect(sut).once.toPromise();

        sut();

        return chai.expect(promise).to.be.fulfilled;
    });

    it("should fulfill the promise created by #toPromise() - passing a number", function() {
        inq.configure(sut).noVerify();
        var promise = inq.expect(sut).thrice.toPromise(2);

        sut();
        sut();

        return chai.expect(promise).to.be.fulfilled;
    });

    it("should be assigned to the mocked object own properties if an inherited method was mocked", function() {
        inq.mock(stub, "inheritedMethod");

        chai.expect(stub).to.have.ownProperty("inheritedMethod");
    });
});
