"use strict";

var chai = require("chai");

var inq = require("../");
var stub = require("../testutils/stub");

describe("sequence", function() {
    it("should work in greedy mode", function() {
        inq.mock(stub, "ownMethod").mode("greedy").noVerify();
        var seq = new inq.Sequence();

        inq.expect(stub.ownMethod).once.args(1).in(seq);
        inq.expect(stub.ownMethod).once.args(2);
        inq.expect(stub.ownMethod).once.args(3).in(seq);

        chai.expect(function() {
            stub.ownMethod(3);
        }).to.throw(inq.ExpectationError);
    });
});
