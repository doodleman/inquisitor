"use strict";

var chai = require("chai");

var inq = require("../");
var cases = require("../testutils/cases");
var forSuts = require("../testutils/forSuts");
var stub = require("../testutils/stub");

describe("#restore()", function() {
    forSuts(["methodMock"], "restore", function(fixture) {
        cases.standard(fixture);

        it("should restore original method", function() {
            fixture.sut.restore();
            chai.expect(fixture.mockedObject[fixture.mockedMethodName])
                .to.be.equal(fixture.originalMethod);
        });

        it("should remove mocking method from owned methods when mocking inherited method", function() {
            var mock = inq.mock(stub, "inheritedMethod");

            mock.restore();
            chai.expect(stub).not.to.have.ownProperty("inheritedMethod");
        });

        it("should not remove or modify the inherited method", function() {
            var originalMethod = stub.inheritedMethod;

            var mock = inq.mock(stub, "inheritedMethod");
            mock.restore();

            chai.expect(stub.constructor.prototype.inheritedMethod).to.equal(originalMethod);
        });
    });

});
