"use strict";

var Promise = require("bluebird");
var chai = require("chai");

var inq = require("../");

var methods = require("./methods");
var notBrokenTypeof = require("./notBrokenTypeof");

module.exports.exists = function(fixture) {
    it("should exist", function() {
        chai.expect(fixture.sut)
            .to.have.property(fixture.mutName)
            .that.is.a("function");
    });
};

module.exports.hasItsAliases = function(fixture) {
    if (Array.isArray(fixture.mut.aliases)) {
        fixture.mut.aliases.forEach(function(alias) {
            it("should have alias '" + alias + "'", function() {
                chai.expect(fixture.sut[fixture.mutName])
                    .to.equal(fixture.sut[alias]);
            });
        });
    }
};

module.exports.isChainable = function(fixture) {
    it("should be chainable", function() {
        chai.expect(methods.callWithExampleArgs(fixture))
            .to.equal(fixture.sut);
    });
};

module.exports.notThrowsWhenCalledCorrectly = function(fixture) {
    it("should not throw if called correctly", function() {
        chai.expect(function() {
            methods.callWithExampleArgs(fixture);
        }).not.to.throw();
    });
};


module.exports.throwsWhenCalledTwice = function(fixture) {
    it("should throw when called twice", function() {
        methods.callWithExampleArgs(fixture);
        chai.expect(function() {
            methods.callWithExampleArgs(fixture);
        }).to.throw(Error, "cannot be set twice.");
    });
};

module.exports.throwsWhenCalledAfterAnyExpectationsWereSet = function(fixture) {
    it("should throw when called after first expectation", function() {
        inq.expect(fixture.mockFunction).once;
        chai.expect(function() {
            methods.callWithExampleArgs(fixture);
        }).to.throw(Error, "must be used before any expectations are set.");
    });
};

module.exports.throwsWhenCalledWithValueOtherThanFunction = function(fixture) {
    [undefined, null, true, 42, "foo"].forEach(function(value) {
        it("should throw when called with " + notBrokenTypeof(value) + " as first parameter", function() {
            chai.expect(function() {
                fixture.sut[fixture.mutName](value);
            }).to.throw(TypeError, "expects function as first parameter.");
        });
    });
};

//todo mockFunction
module.exports.usesExpectationAsDefaultContext = function() {
    it("should use expectation as default context", function() {

    });
};

module.exports.throwsIfCallCountWasNotSet = function(fixture) {
    it("should throw if number of expected calls wasn't set", function() {
        chai.expect(function() {
            methods.callWithExampleArgs(fixture);
        }).to.throw(Error, "cannot be used before number of expected calls is set.");
    });
};

module.exports.throwsIfMinCallCountWasSet = function(fixture) {
    it("should throw if number of minimum expected calls was already set", function() {
        fixture.sut.least(42);

        chai.expect(function() {
            methods.callWithExampleArgs(fixture);
        }).to.throw(Error, "Minimum number of expected calls cannot be set twice.");
    });
};

module.exports.throwsIfMaxCallCountWasSet = function(fixture) {
    it("should throw if number of maximum expected calls was already set", function() {
        fixture.sut.most(42);

        chai.expect(function() {
            methods.callWithExampleArgs(fixture);
        }).to.throw(Error, "Maximum number of expected calls cannot be set twice.");
    });
};

module.exports.throwsIfTestsDidNotStart = function(fixture, errorMessage) {
    it("should throw if called before tests start", function() {
        var state = require("../lib/state");
        state.testsStarted = false; // stubbing

        return Promise.try(function() {
            chai.expect(function() {
                methods.callWithExampleArgs(fixture);
            }).to.throw(Error, errorMessage);
        }).finally(function() { // making sure stub is restored
            state.testsStarted = true; // restore stub
        });
    });
};

module.exports.standard = function(fixture) {
    module.exports.exists(fixture);
    module.exports.hasItsAliases(fixture);
    module.exports.notThrowsWhenCalledCorrectly(fixture);

    if (!fixture.mut.notChainable) {
        module.exports.isChainable(fixture);
    }
};
