"use strict";

var inq = require("../");

var methods = require("./methods");
var stub = require("./stub");

module.exports = function(sutNames, mutName, suite) {
    sutNames.forEach(function(sutName) {
        context("on instance of " + sutName, function() {
            var fixture = {};

            fixture.sutName = sutName;
            fixture.mutName = mutName;
            fixture.mut = methods[sutName][mutName];

            beforeEach(function() {
                if (sutName === "inquisitor") {
                    fixture.sut = inq;
                }
                else {
                    if (sutName === "methodMock") {
                        fixture.mockedObject = stub;
                        fixture.mockedMethodName = "ownMethod";
                        fixture.originalMethod = fixture.mockedObject[fixture.mockedMethodName];
                        inq.mock(fixture.mockedObject, fixture.mockedMethodName);
                        fixture.mockFunction = fixture.mockedObject[fixture.mockedMethodName];
                    }
                    else {
                        fixture.mockFunction = inq.createMockFunction("test mock");
                    }
                    fixture.mock = inq.configure(fixture.mockFunction).noVerify();

                    if (sutName === "mock" || sutName === "methodMock") {
                        fixture.sut = fixture.mock;
                    }
                    else { // "expectation"
                        fixture.sut = inq.expect(fixture.mockFunction);
                        fixture.expectation = fixture.sut;
                    }
                }

            });

            suite(fixture);
        });
    });
};
