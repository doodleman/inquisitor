"use strict";

module.exports = function(value) {
    if (value === null) {
        return "null";
    }
    return (typeof value);
};
