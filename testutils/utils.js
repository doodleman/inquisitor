"use strict";

var inq = require("../");

module.exports.setExpectationIfNotSet = function(fixture, args) {
    if (fixture.expectation === undefined) {
        fixture.expectation = inq.expect(fixture.mockFunction).once.with.args.call(fixture.expectation, args);
    }
};
