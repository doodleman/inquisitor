"use strict";

function Stub() {}

Stub.prototype.inheritedMethod = function() {};

var stub = new Stub;

stub.ownMethod = function() {};
stub.ownNonFunctionProperty = "bar";
stub.ownMethod2 = function() {};

module.exports = stub;
