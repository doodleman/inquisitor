"use strict";

var _ = require("lodash");

var inq = require("../");
var stub = require("./stub");

var inquisitor = {
    mock: {name: "mock", args: [stub, "ownMethod"], notChainable: true},
    mockify: {name: "mockify", args: [stub], notChainable: true},
    isMocked: {name: "isMocked", args: [inq.createMockFunction()], notChainable: true},
    createMockFunction: {name: "createMockFunction", notChainable: true},
    createMockObject: {name: "createMockObject", args: ["foo", "bar"], notChainable: true}
};

var mock = {
    comparator: {name: "comparator", aliases: ["compareWith"], args: [function() {}]},
    return: {name: "return", aliases: ["returns"]},
    callback: {name: "callback", aliases: ["call", "calls"], args: [function() {}]},
    defaultCallback: {name: "defaultCallback", aliases: ["byDefaultCall", "byDefaultCalls"], args: [function() {}]},
    defaultReturn: {name: "defaultReturn", aliases: ["byDefaultReturn", "byDefaultReturns"]},
    verify: {name: "verify"},
    _with: {name: "_with"},
    _set: {name: "_set"},
    _and: {name: "_and"},
    _but: {name: "_but"},
    _which: {name: "_which"}
};

var methodMock = _.extend(mock, {restore: {name: "restore"}});

var expectation = {
    comparator: mock.comparator,
    return: mock.return,
    callback: mock.callback,
    least: {name: "least", args: [7], setsCount: true, minCount: 7},
    _unlimitedly: {name: "_unlimitedly", setsCount: true, minCount: 0},
    most: {name: "most", args: [7], setsCount: true, maxCount: 7},
    between: {name: "between", args: [3, 7], setsCount: true, minCount: 3, maxCount: 7},
    exactly: {name: "exactly", args: [7], count: 7, setsCount: true, minCount: 7, maxCount: 7},
    _never: {name: "_never", count: 0, setsCount: true, minCount: 0, maxCount: 0},
    _once: {name: "_once", count: 1, setsCount: true, minCount: 1, maxCount: 1},
    _twice: {name: "_twice", count: 2, setsCount: true, minCount: 2, maxCount: 2},
    _thrice: {name: "_thrice", count: 3, setsCount: true, minCount: 3, maxCount: 3},
    args: {name: "args"},
    argsWhich: {name: "argsWhich", args: [function() {}]},
    in: {name: "in", args: [new inq.Sequence]},
    toPromise: {name: "toPromise", notChainable: true},
    take: {name: "take", notChainable: true},
    _toBeCalled: {name: "_toBeCalled"},
    _with: {name: "_with"},
    _times: {name: "_times"},
    _at: {name: "_at"},
    _set: {name: "_set"},
    _and: {name: "_and"},
    _but: {name: "_but"},
    _which: {name: "_which"}
};

function callWithExampleArgs(fixture, methodName) {
    if (methodName === undefined) {
        return fixture.sut[fixture.mutName].apply(fixture.sut, fixture.mut.args);
    }
    return fixture.sut[methodName].apply(fixture.sut, module.exports[fixture.sutName][methodName].args);
}

function setsMinCallsCount(method) {
    return _.has(method, "minCount");
}

function setsMaxCallsCount(method) {
    return _.has(method, "maxCount");
}

function setsCallsCount(method) {
    return setsMinCallsCount(method) || setsMaxCallsCount(method);
}

function setsPositiveMinCallsCount(method) {
    return setsMinCallsCount(method) && method.minCount > 0;
}

function getDefaultContext(fixture, originalContext) {
    if (fixture.mutName === "comparator" || fixture.mutName === "argsWhich") {
        return expectation;
    }
    if (fixture.mutName === "callback") {
        return originalContext;
    }
    throw new Error("Method doesn't set any context.");
}

function getDefaultContextName(fixture) {
    if (fixture.mutName === "comparator" || fixture.mutName === "argsWhich") {
        return "expectation object";
    }
    if (fixture.mutName === "callback") {
        return "original context";
    }
    throw new Error("Method doesn't set any context.");
}

module.exports = {
    inquisitor: inquisitor,
    mock: mock,
    methodMock: methodMock,
    expectation: expectation,
    callWithExampleArgs: callWithExampleArgs,
    setsMinCallsCount: setsMinCallsCount,
    setsMaxCallsCount: setsMaxCallsCount,
    setsCallsCount: setsCallsCount,
    setsPositiveMinCallsCount: setsPositiveMinCallsCount,
    getDefaultContext: getDefaultContext,
    getDefaultContextName: getDefaultContextName
};
