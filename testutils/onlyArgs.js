"use strict";

var _ = require("lodash");

// Alternative to classic 'bind' if you don't want bind 'this' but only arguments
module.exports.bind = function(fn) {
    var args = _.toArray(arguments).slice(1);
    return function() {
        return fn.apply(this, args);
    };
};

module.exports.apply = function(fn) {
    var args = _.toArray(arguments).slice(1);
    return function() {
        return fn.apply(this, args);
    };
};

